#!/usr/bin/sh

celery -A django_project worker -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler --beat
