from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from employee.views import (
    mailing_list_list,
    mailing_list_view,
    mailing_list_edit,
    mailing_list_delete,
    ml_add_guest,
    ml_delete_guest,
    mailing_list_export,
    mailing_list_new,
    mailing_list_expiring_view,
    mailing_list_export_all,
)
from django.contrib.auth import views as auth_views
from .api import api


urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "admin/password_reset/",
        auth_views.PasswordResetView.as_view(),
        name="admin_password_reset",
    ),
    path(
        "admin/password_reset/done/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path("djrichtextfield/", include("djrichtextfield.urls")),
    path("accounts/", include("allauth.urls")),
    path("", include("pages.urls")),
    path("employee/", include("employee.urls")),
    path("projects/", include("projects.urls")),
    path("common/", include("common.urls")),
    path(
        "mailinglist/expiring",
        mailing_list_expiring_view,
        name="mailing_list_expiring_view",
    ),
    path("mailinglist/new", mailing_list_new, name="mailing_list_new"),
    path("mailinglist/all", mailing_list_list, name="mailing_list_list"),
    path("mailinglist/<str:ml_id>", mailing_list_view, name="mailing_list_view"),
    path("mailinglist/<str:ml_id>/edit", mailing_list_edit, name="mailing_list_edit"),
    path(
        "mailinglist/<str:ml_id>/delete",
        mailing_list_delete,
        name="mailing_list_delete",
    ),
    path("mailinglist/<str:ml_id>/add", ml_add_guest, name="ml_add_guest"),
    path(
        "mailinglist/<str:ml_id>/<str:id>/delete",
        ml_delete_guest,
        name="ml_delete_guest",
    ),
    path(
        "mailinglist/export/all",
        mailing_list_export_all,
        name="mailing_list_export_all",
    ),
    path(
        "mailinglist/<str:ml_id>/export",
        mailing_list_export,
        name="mailing_list_export",
    ),
    path("contracts/", include("contracts.urls")),
    path("missions/", include("missions.urls")),
    path("department/", include("department.urls")),
    path("api/", api.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
