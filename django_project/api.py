from ninja import NinjaAPI
from ninja.security import APIKeyHeader
from django.conf import settings

class ApiKey(APIKeyHeader):
    param_name = "X-API-Key"

    def authenticate(self, request, key):
        if key == settings.API_KEY:
            return key

header_key = ApiKey()

api = NinjaAPI(auth=header_key)

api.add_router("/department/", "department.api.router")
