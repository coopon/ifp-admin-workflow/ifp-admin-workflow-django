from accounts.models import IFPUser
from projects.models import Project
from common.models import Department
from elasticsearch import Elasticsearch

es = Elasticsearch("http://localhost:9200")
mappings = {
        "properties": {
            "name": {"type": "text"},
            "email": {"type": "text"},
            "designation": {"type": "text"},
            "department": {"type": "text"},
            "project": {"type": "text"},
            "employee_type": {"type": "text"}
            
    }
}
es.indices.create(index="employee", mappings=mappings)

user_qs = IFPUser.objects.all()
user_project = Project.objects.all()
user_dept = Department.objects.all()

for x in range(len(user_qs)):
    name = str(user_qs[x])
    email = user_qs[x].email
    designation = user_qs[x].designation
    employee_type = user_qs[x].employee_type
    rec = {'name':name, 'email': email, 'designation':designation,  'employee_type':employee_type}
    es.index(index="employee", id = x, body=rec)
es.indices.refresh(index="employee")
es.cat.count(index="employee", format="json")
"""
search_obj = {
 "query": {
 "query_string": {
 "email": "noor"
 }
 },
 "filter": {
 "term": { "designation": "Technical Agent" }
       }
     }


resp = es.search(index="employee", body=search_obj)

"""