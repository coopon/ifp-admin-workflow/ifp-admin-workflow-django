# Generated by Django 4.2 on 2023-05-16 11:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("contracts", "0013_contract_contract_signed_copy_alter_contract_status"),
    ]

    operations = [
        migrations.RenameField(
            model_name="contract",
            old_name="name",
            new_name="first_name",
        ),
        migrations.AddField(
            model_name="contract",
            name="gender",
            field=models.CharField(
                blank=True,
                choices=[
                    ("Male", "Male"),
                    ("Female", "Female"),
                    ("Transgender", "Transgender"),
                ],
                max_length=20,
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="contract",
            name="last_name",
            field=models.CharField(default="random", max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="contract",
            name="passport_name",
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name="contract",
            name="passport_number",
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name="contract",
            name="title",
            field=models.CharField(
                blank=True,
                choices=[("Mr.", "Mr."), ("Ms.", "Ms."), ("Mrs.", "Mrs.")],
                max_length=10,
                null=True,
            ),
        ),
        migrations.AlterField(
            model_name="contract",
            name="status",
            field=models.CharField(
                choices=[
                    ("Proposed", "Proposed"),
                    ("Acknowledged", "Acknowledged"),
                    ("Employee Submitted", "Employee Submitted"),
                    ("Employee Resubmit", "Employee Resubmit"),
                    ("Contract Drafting", "Contract Drafting"),
                    ("Contract Redrafting", "Contract Redrafting"),
                    ("Contract Draft Review", "Contract Draft Review"),
                    ("Contract Drafted", "Contract Drafted"),
                    ("Signed", "Signed"),
                    ("Completed", "Completed"),
                ],
                default="Proposed",
                max_length=50,
            ),
        ),
    ]
