from django.urls import path

from contracts.views import *

urlpatterns = [
    path("new", new_contract, name="contracts_new"),
    path("amend", amend_contract, name="amend_contract"),
    path("list", contracts_list, name="contracts_list"),
    path("<uuid:con_id>/view", contracts_view, name="contracts_view"),
    path("<uuid:con_id>/edit", contracts_edit, name="contracts_edit"),
    path("<uuid:con_id>/upload", upload_signed_copy, name="upload_signed_copy"),
    path("<uuid:con_id>/start_contract", start_contract, name="start_contract"),
    path(
        "<uuid:con_id>/view_proposed",
        contracts_proposed_view,
        name="contracts_proposed_view",
    ),
    path("<uuid:con_id>/validate", contracts_validate, name="contracts_validate"),
    path("<uuid:con_id>/resubmit", contract_resubmit, name="contract_resubmit"),
    # Contract PDF
    # path("<uuid:con_id>/pdf", contract_render_pdf, name="contract_render_pdf"),
    path(
        "<uuid:con_id>/final_contract_pdf",
        final_contract_pdf,
        name="final_contract_pdf",
    ),
    # Contract Drafting
    path(
        "<uuid:con_id>/contract_drafting", contract_drafting, name="contract_drafting"
    ),
    path(
        "<uuid:con_id>/contract_drafting/chrono_number",
        add_chrono_number,
        name="add_chrono_number",
    ),
    path(
        "<uuid:con_id>/contract_drafting/section/new",
        contract_drafting_section_new,
        name="contract_drafting_section_new",
    ),
    path(
        "<uuid:con_id>/contract_drafting/section/<int:sec_id>/edit",
        contract_drafting_section_edit,
        name="contract_drafting_section_edit",
    ),
    path(
        "<uuid:con_id>/contract_drafting/section/<int:sec_id>/delete",
        contract_drafting_section_delete,
        name="contract_drafting_section_delete",
    ),
    # Contract OnBoarding
    path("<uuid:con_id>/onboard", contract_onboard, name="contract_onboard"),
    path(
        "<uuid:con_id>/onboard_preview",
        contract_onboard_preview,
        name="contract_onboard_preview",
    ),
    path(
        "<uuid:con_id>/onboard_submit",
        contract_onboard_submit,
        name="contract_onboard_submit",
    ),
    # Contract template URLS
    path("contract_template/new", contract_template_new, name="contract_template_new"),
    path(
        "contract_template/list",
        contract_templates_list,
        name="contract_templates_list",
    ),
    path(
        "contract_template/<uuid:con_id>",
        contract_template_view,
        name="contract_template_view",
    ),
    path(
        "contract_template/<uuid:con_id>/edit",
        contract_template_edit,
        name="contract_template_edit",
    ),
    path(
        "contract_template/<uuid:con_id>/delete",
        contract_template_delete,
        name="contract_template_delete",
    ),
    path(
        "contract_template/<uuid:con_id>/section/new",
        contract_template_section_new,
        name="contract_template_section_new",
    ),
    path(
        "contract_template/<uuid:con_id>/section/<int:sec_id>/delete",
        contract_template_section_delete,
        name="contract_template_section_delete",
    ),
    path(
        "contract_template/<uuid:con_id>/section/<int:sec_id>/edit",
        contract_template_section_edit,
        name="contract_template_section_edit",
    ),
]
