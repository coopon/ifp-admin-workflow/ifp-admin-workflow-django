from django.shortcuts import render, redirect, reverse
from datetime import datetime, timedelta
from common.models import CalendarLeaves
from department.models import Department
from contracts.models import (
    Contract,
    AuditTrail,
    ContractTemplate,
    SectionTemplate,
    ContractSection,
    ContractHistory,
)
from contracts.forms.contract import (
    NewContractForm,
    UploadSignedCopy,
    ContractResubmitForm,
    SetContractTypeForm,
    OnBoardPersonalDetails,
    OnBoardDependentDetails,
    OnBoardEmergencyContact,
    OnBoardEducationDetails,
    OnBoardAffiliatedInstitution,
    OnBoardVisa,
    OnBoardBankAccount,
    ContractSectionForm,
    AmendContractForm,
    ChronoNumberForm,
)
from contracts.forms.contract_template import (
    ContractTemplateForm,
    ContractSectionTemplateForm,
)
from accounts.models import IFPUser, BankAccount
from employee.forms import (
    OnBoardPersonalFrom,
    OnBoardDependentForm,
    OnBoardEmergencyContactForm,
    OnBoardAffiliatedInstitutionForm,
    OnBoardEducationDetailsForm,
    OnBoardEmployeeDetailsForm,
    OnBoardVisa,
    LeaveTopUp,
)
from django.shortcuts import get_list_or_404, get_object_or_404
from django.views.decorators.http import require_http_methods
from accounts.tasks import send_mail_func
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
import io as BytesIO
from common.utils import link_callback
from django.template.loader import render_to_string
from projects.models import Project
from common.models import Config
from django.conf import settings
import os


@login_required
def new_contract(request):
    error_code = 0
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        contract = Contract()
        form = NewContractForm(request.POST, instance=contract)
        if form.is_valid():
            contract = form.save(commit=False)
            proposer = IFPUser.objects.get(id=request.user.id)
            contract.proposer = proposer
            project_name = form.cleaned_data["project"]
            budget = form.cleaned_data["budget"]
            contract_from_date = form.cleaned_data["from_date"]
            contract_to_date = form.cleaned_data["to_date"]
            project = Project.objects.get(name=project_name)
            if budget > project.budget:
                error_code = 1
                return render(
                    request,
                    "contracts/new_contract_form.html",
                    {
                        "contract_form": form,
                        "error_code": error_code,
                        "employee": employee,
                    },
                )
            if project.to_date:
                if contract_to_date > project.to_date:
                    error_code = 2
                    return render(
                        request,
                        "contracts/new_contract_form.html",
                        {
                            "contract_form": form,
                            "error_code": error_code,
                            "employee": employee,
                        },
                    )
            if contract_from_date > contract_to_date:
                error_code = 3
                return render(
                    request,
                    "contracts/new_contract_form.html",
                    {
                        "contract_form": form,
                        "error_code": error_code,
                        "employee": employee,
                    },
                )
            contract.save()
            full_name = str(
                IFPUser.objects.get(id=request.user.id).first_name
                + " "
                + IFPUser.objects.get(id=request.user.id).last_name
            )
            audit_trail = AuditTrail.objects.create(
                audit_status="Proposed",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "New contract is proposed by "
                    + str(contract.proposer)
                    + " for the project of "
                    + contract.project.name
                    + " From "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " with budget of "
                    + str(contract.budget)
                    + " is waiting for acknowledgement",
                    "subject": "Opera - Contract Proposed",
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            return redirect(reverse("contracts_list"))
        else:
            print(form.errors)
            return render(
                request,
                "contracts/new_contract_form.html",
                {"contract_form": form, "error_code": error_code, "employee": employee},
            )
    else:
        #        if len(Project.objects.filter(request.user.id)) == 0:
        #            return redirect(reverse("contracts_list"))
        form = NewContractForm(proposer=request.user)
        #        contracts = Contract.objects.all()
        return render(
            request,
            "contracts/new_contract_form.html",
            {"contract_form": form, "error_code": error_code, "employee": employee},
        )


def contract_onboard(request, con_id):
    contract = Contract.objects.get(id=con_id)
    if contract.status == "Employee Submitted" or contract.status not in [
        "Acknowledged",
        "Employee Resubmit",
    ]:
        return redirect(reverse("contract_onboard_submit", args=[con_id]))
    forms = {
        "personal_details": OnBoardPersonalDetails(instance=contract),
        "dependent_details": OnBoardDependentDetails(instance=contract),
        "emergency_contact": OnBoardEmergencyContact(instance=contract),
        "education_details": OnBoardEducationDetails(instance=contract),
        "affiliated_institution": OnBoardAffiliatedInstitution(instance=contract),
        "visa": OnBoardVisa(instance=contract),
        "bank_account": OnBoardBankAccount(instance=contract),
    }
    if request.method == "POST":
        for file_field in request.FILES.keys():
            field_file = getattr(contract, file_field)
            field_file.delete(save=False)
        forms = {
            "personal_details": OnBoardPersonalDetails(
                request.POST, request.FILES, instance=contract
            ),
            "emergency_contact": OnBoardEmergencyContact(
                request.POST, request.FILES, instance=contract
            ),
            "bank_account": OnBoardBankAccount(
                request.POST, request.FILES, instance=contract
            ),
        }
        if forms["personal_details"].is_valid():
            contract = forms["personal_details"].save()
        if contract.contract_type.get_dependent_for_insurance:
            forms["dependent_details"] = OnBoardDependentDetails(
                request.POST, request.FILES, instance=contract
            )
            if forms["dependent_details"].is_valid():
                contract = forms["dependent_details"].save()
        if forms["emergency_contact"].is_valid():
            contract = forms["emergency_contact"].save()
        if forms["bank_account"].is_valid():
            contract = forms["bank_account"].save()
        if contract.contract_type.get_educational_details:
            forms["education_details"] = OnBoardEducationDetails(
                request.POST, request.FILES, instance=contract
            )
            if forms["dependent_details"].is_valid():
                contract = forms["dependent_details"].save()
        if contract.affiliated_to_institution:
            forms["affiliated_institution"] = OnBoardAffiliatedInstitution(
                request.POST, request.FILES, instance=contract
            )
            if forms["affiliated_institution"].is_valid():
                contract = forms["affiliated_institution"].save()
        if contract.nationality.name != "India":
            forms["visa"] = OnBoardVisa(request.POST, request.FILES, instance=contract)
            if forms["visa"].is_valid():
                contract = forms["visa"].save()
        if request.POST.get("preview"):
            return redirect(reverse("contract_onboard_preview", args=[con_id]))
        contract.resubmit_reason = None
        contract.save()
        return redirect(reverse("contract_onboard", args=[con_id]))
    return render(
        request,
        "contracts/contract_onboard.html",
        {"forms": forms, "contract": contract},
    )


def contract_onboard_preview(request, con_id):
    contract = Contract.objects.get(id=con_id)
    return render(
        request, "contracts/contract_onboard_preview.html", {"contract": contract}
    )


def contract_onboard_submit(request, con_id):
    contract = Contract.objects.get(id=con_id)
    message = "Your application is under processing"
    if contract.status == "Employee Submitted":
        message = "Already Submitted the details. Please wait for your application to get processed."
    elif contract.status == "Acknowledged" or contract.status == "Employee Resubmit":
        contract.status = "Employee Submitted"
        audit_trail = AuditTrail.objects.create(
            audit_status="Employee Submitted",
            contract=contract,
            changed_by=str(contract.first_name + " " + contract.last_name),
            changed_at=datetime.now(),
        )
        config = Config.objects.all().last()
        contract_email_rep = [config.admin.email, config.gensec.email]
        send_mail_func.apply_async(
            kwargs={
                "message": contract.first_name
                + " "
                + contract.last_name
                + " in Project "
                + contract.project.name
                + " from "
                + str(contract.from_date)
                + " till "
                + str(contract.to_date)
                + " has submitted the onboard form.",
                "subject": "Opera - Onboard form submitted",
                "to_email": contract_email_rep,
                "action_link": "https://opera.ifpindia.org/contracts/list",
            }
        )
        audit_trail.save()
        contract.save()
        message = "Thank you for sharing the details. Administration will get back to you soon after processing your application."
    return render(
        request,
        "contracts/contract_onboard_submit.html",
        {"contract": contract, "message": message},
    )


@login_required
def contracts_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    form = UploadSignedCopy()
    return render(
        request,
        "contracts/contracts_list.html",
        {
            "contracts_proposed": Contract.objects.filter(status="Proposed"),
            "contracts_waiting_for_info": Contract.objects.filter(status="Acknowledged")
            | Contract.objects.filter(status="Employee Resubmit"),
            "contracts_under_validation": Contract.objects.filter(
                status="Employee Submitted"
            ),
            "contracts_contract_drafting": Contract.objects.filter(
                status="Contract Drafting"
            )
            | Contract.objects.filter(status="Contract Redrafting"),
            "contracts_contract_draft_review": Contract.objects.filter(
                status="Contract Draft Review"
            ),
            "contracts_proposer_review": Contract.objects.filter(
                status="Contract Drafted"
            ),
            "contracts_to_be_signed": Contract.objects.filter(
                status="Proposer Acknowledged"
            ),
            "form": form,
            "employee": employee,
            "contracts_signed": Contract.objects.filter(status="Signed"),
            "contracts_completed": Contract.objects.filter(status="Completed"),
        },
    )


@login_required
def upload_signed_copy(request, con_id):
    contract = Contract.objects.get(id=con_id)
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        form = UploadSignedCopy(request.POST, request.FILES, instance=contract)
        if form.is_valid():
            # contract.contract_signed_copy = form.cleaned_data['contract_signed_copy']
            contract.status = "Signed"
            audit_trail = AuditTrail.objects.create(
                audit_status="Signed",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": full_name
                    + " have uploaded the signed copy of contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + ".",
                    "subject": "Opera - Contract Signed",
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            form.save()
            contract.save()
            return redirect(reverse("contracts_list"))


@login_required
def start_contract(request, con_id):
    contract = Contract.objects.get(id=con_id)
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    count = IFPUser.objects.count()
    empid = "EMP - " + str("{:04d}".format(count + 1))
    # Personal details
    employee = IFPUser.objects.create(
        emp_id=empid,
        email=contract.email,
        first_name=contract.first_name,
        last_name=contract.last_name,
    )
    employee = IFPUser.objects.get(emp_id=empid)
    employee.title = contract.title
    employee.father_name = contract.father_name
    employee.doj = contract.from_date
    employee.gender = contract.gender
    employee.dob = contract.date_of_birth
    employee.phone = contract.phone_number
    employee.perm_addr = contract.permanent_address
    employee.comm_addr = contract.current_address
    employee.display_picture = contract.recent_photo
    employee.nationality.code = contract.nationality
    employee.supervisor = contract.proposer
    # Contract details
    employee.contract_from = contract.from_date
    employee.contract_to = contract.to_date
    employee.contract_type = contract.contract_type.type
    # Affilation details
    if contract.affiliated_to_institution:
        employee.affiliated_to_institution = contract.affiliated_to_institution
        employee.affiliated_institution = contract.affiliated_institution
        employee.affiliated_institution_dor = contract.affiliated_institution_dor
        employee.affiliated_institution_email = contract.affiliated_institution_email
        employee.affiliated_institution_id = contract.affiliated_institution_id
    # Education details
    if contract.contract_type.get_educational_details:
        employee.get_educational_details = (
            contract.contract_type.get_educational_details
        )
        employee.institute_name = contract.institute_name
        employee.degree_name = contract.degree_name
        employee.year_of_passing = contract.year_of_passing
        employee.mark_percent = contract.mark_percent
        employee.degree_certificate = contract.degree_certificate
    # Bank details
    if contract.contract_type.get_bank_details:
        employee.get_bank_details = contract.contract_type.get_bank_details
        bank_acc = BankAccount.objects.create(uid=contract.uid)
        bank_acc.acc_name = contract.acc_name
        bank_acc.acc_no = contract.acc_no
        bank_acc.acc_type = contract.acc_type
        bank_acc.uid_type = contract.uid_type
        bank_acc.bank_name = contract.bank_name
        bank_acc.branch = contract.branch
        bank_acc.bank_name = contract.bank_name
        bank_acc.pan = contract.pan
        bank_acc.bank_details_proof = contract.bank_details_proof
        bank_acc.save()
        employee.bank_accounts.add(bank_acc)
    # Dependent details
    if contract.contract_type.get_dependent_for_insurance:
        employee.get_dependent_for_insurance = (
            contract.contract_type.get_dependent_for_insurance
        )
        employee.spouse_name = contract.spouse_name
        employee.spouse_dob = contract.spouse_dob
        employee.spouse_id_proof = contract.spouse_id_proof
        employee.child1_name = contract.child1_name
        employee.child1_dob = contract.child1_dob
        employee.child1_id_proof = contract.child1_id_proof
        employee.child2_name = contract.child2_name
        employee.child2_dob = contract.child2_dob
        employee.child2_id_proof = contract.child2_id_proof
    # Emergency Details
    employee.emergency_contact_name = contract.emergency_contact_name
    employee.emergency_contact_mobile = contract.emergency_contact_mobile
    employee.emergency_contact_relationship = contract.emergency_contact_relationship
    employee.affiliated_institution = contract.affiliated_institution
    employee.affiliated_institution_email = contract.affiliated_institution_email
    # VISA Details
    if contract.nationality.name != "India":
        employee.visa_from = contract.visa_start_date
        employee.visa_to = contract.visa_end_date
        employee.passport_name = contract.passport_name
        employee.passport_number = contract.passport_number
    contract.status = "Completed"
    audit_trail = AuditTrail.objects.create(
        audit_status="Completed",
        contract=contract,
        changed_by=full_name,
        changed_at=datetime.now(),
    )
    audit_trail.save()
    config = Config.objects.all().last()
    contract_email_rep = [config.admin.email, config.gensec.email]
    send_mail_func.apply_async(
        kwargs={
            "message": contract.first_name
            + " "
            + contract.last_name
            + " in Project "
            + contract.project.name
            + " from "
            + str(contract.from_date)
            + " till "
            + str(contract.to_date)
            + " has been onboarded.",
            "subject": "Opera - Employee Onboarded",
            "to_email": contract_email_rep,
            "action_link": "https://opera.ifpindia.org/contracts/list",
        }
    )
    contract.save()
    employee.save()
    # Project
    project = Project.objects.get(name=contract.project.name)
    project.members.add(employee)
    # Add PI in Project
    pi = contract.proposer
    count = 0
    for pro in pi.project_members.all():
        if pro == project:
            count = 1
    if count == 0:
        project.members.add(pi)
    project.save()
    # Department
    dept = Department.objects.get(name=contract.department.name)
    dept.department_members.add(employee)
    dept.save()
    # Contract history
    contract_history = ContractHistory.objects.create(
        contract=contract, employee=employee
    )
    contract_history.save()
    forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(instance=employee),
        "OnBoardDependentForm": OnBoardDependentForm(instance=employee),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(instance=employee),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(
            instance=employee
        ),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(instance=employee),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(instance=employee),
        "OnBoardVisa": OnBoardVisa(instance=employee),
    }
    leave_topup = LeaveTopUp(instance=employee)
    return render(
        request,
        "employee/edit_details.html",
        {
            "employee": employee,
            "emp": employee,
            "forms": forms,
            "topup_form": leave_topup,
        },
    )


@login_required
def contracts_proposed_view(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser or employee.pi == True:
        contract = Contract.objects.get(id=con_id)
        contract_type_form = SetContractTypeForm()
        return render(
            request,
            "contracts/contracts_view.html",
            {
                "contract": contract,
                "contract_type_form": contract_type_form,
                "employee": employee,
            },
        )


@login_required
def contracts_view(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser or employee.pi == True:
        contract = Contract.objects.get(id=con_id)
        audit_trails = AuditTrail.objects.filter(contract=contract).order_by(
            "changed_at"
        )
        return render(
            request,
            "contracts/contracts_validate.html",
            {"contract": contract, "audit_trails": audit_trails, "employee": employee},
        )


@login_required
def contracts_validate(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract = Contract.objects.get(id=con_id)
    audit_trails = AuditTrail.objects.filter(contract=contract).order_by("changed_at")
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        if request.POST.get("action") == "validate":
            contract.status = "Contract Drafting"
            audit_trail = AuditTrail.objects.create(
                audit_status="Contract Drafting",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "Contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " has sent to contract drafting by "
                    + full_name
                    + ". ",
                    "subject": "Opera - " + contract.status,
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            contract.save()
            return redirect(reverse("contracts_list"))
    return render(
        request,
        "contracts/contracts_validate.html",
        {"contract": contract, "audit_trails": audit_trails, "employee": employee},
    )


# Contract Drafting
@login_required
def contract_drafting(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract = Contract.objects.get(id=con_id)
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    if contract.status not in [
        "Contract Drafting",
        "Contract Redrafting",
        "Contract Draft Review",
    ]:
        return redirect(reverse("contracts_list"))
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        if request.POST.get("action") == "contract_review":
            contract.status = "Contract Draft Review"
            audit_trail = AuditTrail.objects.create(
                audit_status="Contract Draft Review",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "Contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " has sent to contract draft review by "
                    + full_name
                    + ". ",
                    "subject": "Opera - " + contract.status,
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            contract.save()
            return redirect(reverse("contracts_list"))
        if request.POST.get("action") == "contract_redraft":
            contract.status = "Contract Redrafting"
            audit_trail = AuditTrail.objects.create(
                audit_status="Contract Redrafting",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "Contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " has sent to contract redrafting by "
                    + full_name
                    + ".",
                    "subject": "Opera - " + contract.status,
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            contract.save()
            return redirect(reverse("contracts_list"))
        if request.POST.get("action") == "contract_verified":
            contract.status = "Contract Drafted"
            audit_trail = AuditTrail.objects.create(
                audit_status="Contract Drafted",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "Contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " has been draftedby "
                    + full_name
                    + ".",
                    "subject": "Opera - " + contract.status,
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            contract.save()
            return redirect(reverse("contracts_list"))
    sections_fr = ContractSection.objects.filter(
        contract=contract, language="fr"
    ).order_by("order")
    sections_en = ContractSection.objects.filter(
        contract=contract, language="en"
    ).order_by("order")
    return render(
        request,
        "contracts/contract_drafting.html",
        {
            "contract": contract,
            "sections_fr": sections_fr,
            "sections_en": sections_en,
            "employee": employee,
        },
    )


@login_required
def contract_drafting_section_new(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract = Contract.objects.get(id=con_id)
    if contract.status not in [
        "Contract Drafting",
        "Contract Redrafting",
        "Contract Draft Review",
    ]:
        return redirect(reverse("contracts_list"))
    if request.method == "POST":
        section = ContractSection()
        contract_section_form = ContractSectionForm(request.POST, instance=section)
        if contract_section_form.is_valid():
            section = contract_section_form.save(commit=False)
            section.contract = contract
            section.save()
            return redirect(reverse("contract_drafting", args=[con_id]))
        else:
            return redirect(reverse("contract_drafting_section_new", args=[con_id]))
    else:
        contract_section_form = ContractSectionForm()
        return render(
            request,
            "contracts/contract_drafting_section_new.html",
            {
                "contract": contract,
                "section_form": contract_section_form,
                "employee": employee,
            },
        )


def contract_drafting_section_edit(request, con_id, sec_id):
    contract = Contract.objects.get(id=con_id)
    employee = IFPUser.objects.get(id=request.user.id)
    if contract.status not in [
        "Contract Drafting",
        "Contract Redrafting",
        "Contract Draft Review",
    ]:
        return redirect(reverse("contracts_list"))
    section = ContractSection.objects.get(id=sec_id)
    if section.contract == contract:
        if request.method == "POST" and (
            request.user.is_superuser or employee.pi == True
        ):
            contract_section_form = ContractSectionForm(request.POST, instance=section)
            if contract_section_form.is_valid():
                contract_section_form.save()
                return redirect(reverse("contract_drafting", args=[con_id]))
            return redirect(
                reverse("contract_drafting_section_edit", args=[con_id, sec_id])
            )
        else:
            contract_section_form = ContractSectionForm(instance=section)
            return render(
                request,
                "contracts/contract_drafting_section_edit.html",
                {
                    "contract": contract,
                    "section": section,
                    "section_form": contract_section_form,
                    "employee": employee,
                },
            )
    return redirect(reverse("contract_drafting", args=[con_id]))


@login_required
def add_chrono_number(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract = Contract.objects.get(id=con_id)
    if contract.status not in [
        "Contract Drafting",
        "Contract Redrafting",
        "Contract Draft Review",
    ]:
        return redirect(reverse("contracts_list"))
    if request.method == "POST":
        chrono_number_form = ChronoNumberForm(request.POST, instance=contract)
        if chrono_number_form.is_valid():
            chrono_number_form.save()
            return redirect(reverse("contract_drafting", args=[con_id]))
    else:
        chrono_number_form = ChronoNumberForm(instance=contract)
    return render(
        request,
        "contracts/add_chrono_number.html",
        {
            "employee": employee,
            "contract": contract,
            "chrono_number_form": chrono_number_form,
        },
    )


def contract_drafting_section_delete(request, con_id, sec_id):
    contract = Contract.objects.get(id=con_id)
    section = ContractSection.objects.get(id=sec_id)
    if section.contract == contract:
        section.delete()
    return redirect(reverse("contract_drafting", args=[con_id]))


@login_required
def contracts_edit(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    if request.method == "POST" and "status" in request.POST:
        contract = Contract.objects.get(id=con_id)
        contract_type_form = SetContractTypeForm(request.POST, instance=contract)
        if request.POST["status"] != "Acknowledged":
            contract_form = contract_type_form.save(commit=False)
            contract_form.save()
        section_templates = SectionTemplate.objects.filter(
            contract_template=contract.contract_type
        )
        for section in section_templates:
            contract_section = ContractSection(
                order=section.order,
                tittle=section.tittle,
                description=section.description,
                language=section.language,
                contract=contract,
            )
            contract_section.save()
        status = request.POST["status"]
        if status != contract.status:
            refund_dict = {
                value: key for key, value in contract.CONTRACT_STATUS_CHOICES
            }
            ind = list(refund_dict.keys()).index(contract.status)
            new_status = list(refund_dict.keys())[ind + 1]
            contract.status = new_status
            audit_trail = AuditTrail.objects.create(
                audit_status=new_status,
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "Contract of "
                    + contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " has sent to "
                    + contract.status
                    + " by "
                    + full_name
                    + ".",
                    "subject": "Opera - " + contract.status,
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            contract.save()
            if contract.amend and contract.status == "Acknowledged":
                contract.status = "Employee Submitted"
            else:
                send_mail_func.apply_async(
                    kwargs={
                        "message": "Welcome to French Institute of Pondicherry. Your Contract is Acknowledged Successfully. We are happy to onboard you.\n Please fill up the form below to comfirm your details.\n\nThank you.",
                        "subject": "Opera - Acknowledgement",
                        "subject": "Welcome to IFP",
                        "to_email": [contract.email],
                        "action_link": "https://opera.ifpindia.org/contracts/"
                        + str(contract.id)
                        + "/onboard",
                        "action_text": "Click here to fill up the form",
                        "first_name": str(contract.first_name),
                    }
                )
            contracts = Contract.objects.all()
            return redirect(reverse("contracts_list"))
        else:
            contract = Contract.objects.get(id=con_id)
            error = {"error": "Already in same status"}
            return render(
                request,
                "contracts/contracts_view.html",
                {"contract": contract, "error": error, "employee": employee},
            )
    elif request.method == "POST" and "contract_type" in request.POST:
        contract = Contract.objects.get(id=con_id)
        contract_type_form = SetContractTypeForm(request.POST, instance=contract)
        contract = contract_type_form.save(commit=False)
        section_templates = SectionTemplate.objects.filter(
            contract_template=contract.contract_type
        )
        for section in section_templates:
            contract_section = ContractSection(
                order=section.order,
                tittle=section.tittle,
                description=section.description,
                language=section.language,
                contract=contract,
            )
            contract_section.save()
        audit_trail = AuditTrail.objects.create(
            audit_status="Contract type added",
            contract=contract,
            changed_by=full_name,
            changed_at=datetime.now(),
        )
        audit_trail.save()
        config = Config.objects.all().last()
        contract_email_rep = [config.admin.email, config.gensec.email]
        send_mail_func.apply_async(
            kwargs={
                "message": " Contract type is added for "
                + "Contract of "
                + contract.first_name
                + " "
                + contract.last_name
                + " in Project "
                + contract.project.name
                + " from "
                + str(contract.from_date)
                + " till "
                + str(contract.to_date)
                + " by "
                + full_name
                + ".",
                "subject": "Opera - contract type added",
                "to_email": contract_email_rep,
                "action_link": "https://opera.ifpindia.org/contracts/list",
            }
        )
        contract.save()
        return redirect(reverse("contracts_proposed_view", args=[con_id]))

    else:
        contract = Contract.objects.get(id=con_id)
        form = NewContractForm(request.POST, instance=contract)
        if form.is_valid():
            form.save()
        return redirect("/contracts/")


@login_required
def contract_resubmit(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract = Contract.objects.get(id=con_id)
    full_name = str(
        IFPUser.objects.get(id=request.user.id).first_name
        + " "
        + IFPUser.objects.get(id=request.user.id).last_name
    )
    if request.method == "GET":
        form = ContractResubmitForm(instance=contract)
        return render(
            request,
            "contracts/contract_resubmit.html",
            {"form": form, "contract": contract, "employee": employee},
        )
    else:
        form = ContractResubmitForm(request.POST, instance=contract)
        if form.is_valid():
            contract.status = "Employee Resubmit"
            audit_trail = AuditTrail.objects.create(
                audit_status="Employee Resubmit",
                contract=contract,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": contract.first_name
                    + " "
                    + contract.last_name
                    + " in Project "
                    + contract.project.name
                    + " from "
                    + str(contract.from_date)
                    + " till "
                    + str(contract.to_date)
                    + " is asked to resubmit the onboard form.",
                    "subject": "Opera - Asked to resubmit the onboard form",
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            send_mail_func.apply_async(
                kwargs={
                    "message": " You are asked to resubmit the onboard form.",
                    "subject": "Welcome to IFP - Resubmit the onboard Form",
                    "to_email": [contract.email],
                    "action_link": "https://opera.ifpindia.org/contracts/"
                    + str(contract.id)
                    + "/onboard",
                    "action_text": "Click here to fill up the form",
                    "reject_reason": str(contract.resubmit_reason),
                    "first_name": str(contract.first_name),
                }
            )
            contract.resubmit_reason = form.cleaned_data["resubmit_reason"]
            contract.save()
            return redirect(reverse("contracts_list"))


# Contracts Templates Views
@login_required
def contract_template_new(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        contract_template = ContractTemplate()
        form = ContractTemplateForm(request.POST, instance=contract_template)
        if form.is_valid:
            contract_template = form.save()
            contract_template.save()
            return redirect(reverse("contract_templates_list"))
        else:
            return redirect(reverse("contract_template_new"))
    else:
        form = ContractTemplateForm()
        return render(
            request,
            "contracts/new_contract_template_form.html",
            {"contract_template_form": form, "employee": employee},
        )


@login_required
def contract_templates_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser or employee.pi == True:
        contract_templates = ContractTemplate.objects.all()
        return render(
            request,
            "contracts/contract_templates_list.html",
            {"contract_templates": contract_templates, "employee": employee},
        )


@login_required
def contract_template_delete(request, con_id):
    if request.user.is_superuser:
        contract_template = ContractTemplate.objects.get(id=con_id)
        contract_template.delete()
        return redirect(reverse("contract_templates_list"))


@login_required
def contract_template_edit(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract_template = ContractTemplate.objects.get(id=con_id)
    if request.method == "GET" and (request.user.is_superuser or employee.pi == True):
        form = ContractTemplateForm(instance=contract_template)
        return render(
            request,
            "contracts/contract_template_edit.html",
            {
                "form": form,
                "contract_template": contract_template,
                "employee": employee,
            },
        )
    else:
        form = ContractTemplateForm(request.POST, instance=contract_template)
        if form.is_valid():
            form.save()
            return redirect(reverse("contract_templates_list"))


@login_required
def contract_template_view(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser or employee.pi == True:
        contract_template = ContractTemplate.objects.get(id=con_id)
        sections_fr = SectionTemplate.objects.filter(
            contract_template=contract_template, language="fr"
        ).order_by("order")
        sections_en = SectionTemplate.objects.filter(
            contract_template=contract_template, language="en"
        ).order_by("order")
        return render(
            request,
            "contracts/contract_template_view.html",
            {
                "contract_template": contract_template,
                "sections_fr": sections_fr,
                "sections_en": sections_en,
                "employee": employee,
            },
        )


# Sections Views
@login_required
def contract_template_section_new(request, con_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST" and (request.user.is_superuser or employee.pi == True):
        section = SectionTemplate()
        contract_template = ContractTemplate.objects.get(id=con_id)
        contract_template_section_form = ContractSectionTemplateForm(
            request.POST, instance=section
        )
        if contract_template_section_form.is_valid():
            section = contract_template_section_form.save(commit=False)
            section.contract_template = contract_template
            section.save()
            return redirect(reverse("contract_template_view", args=[con_id]))
        else:
            return redirect(reverse("contract_template_section_new", args=[con_id]))
    else:
        contract_template = ContractTemplate.objects.get(id=con_id)
        contract_template_section_form = ContractSectionTemplateForm()
        return render(
            request,
            "contracts/contract_template_section_new.html",
            {
                "contract_template": contract_template,
                "section_form": contract_template_section_form,
                "employee": employee,
            },
        )


@login_required
def contract_template_section_delete(request, con_id, sec_id):
    contract_template = ContractTemplate.objects.get(id=con_id)
    section = SectionTemplate.objects.get(id=sec_id)
    if section.contract_template == contract_template:
        section.delete()
    return redirect(reverse("contract_template_view", args=[con_id]))


@login_required
def contract_template_section_edit(request, con_id, sec_id):
    employee = IFPUser.objects.get(id=request.user.id)
    contract_template = ContractTemplate.objects.get(id=con_id)
    section = SectionTemplate.objects.get(id=sec_id)
    if section.contract_template == contract_template:
        if request.method == "POST" and (
            request.user.is_superuser or employee.pi == True
        ):
            contract_template_section_form = ContractSectionTemplateForm(
                request.POST, instance=section
            )
            if contract_template_section_form.is_valid():
                contract_template_section_form.save()
                return redirect(reverse("contract_template_view", args=[con_id]))
            return redirect(
                reverse("contract_template_section_edit", args=[con_id, sec_id])
            )
        else:
            contract_template_section_form = ContractSectionTemplateForm(
                instance=section
            )
            return render(
                request,
                "contracts/contract_template_section_edit.html",
                {
                    "contract_template": contract_template,
                    "section": section,
                    "section_form": contract_template_section_form,
                    "employee": employee,
                },
            )
    return redirect(reverse("contract_template_view", args=[con_id]))


# def contract_render_pdf(request, con_id):
#     contract = Contract.objects.get(id=con_id)
#     sections_fr = ContractSection.objects.filter(
#         contract=contract, language="fr"
#     ).order_by("order")
#     sections_en = ContractSection.objects.filter(
#         contract=contract, language="en"
#     ).order_by("order")
#     doj = contract.from_date - timedelta(days=7)
#     issue_date = contract_sign_date(doj)
#     template = get_template("contracts/final_contract_pdf.html")
#     context_dict = {
#         "contract": contract,
#         "sections_fr": sections_fr,
#         "sections_en": sections_en,
#         "issue_date": issue_date,
#     }

#     pdf = pdfkit.from_url(
#         "http://127.0.0.1:8000/contracts/365c0ce2-6110-4549-a57b-8f82a5891810/final_contract_pdf",
#         False,
#         options={
#             "enable-local-file-access": True,
#             "footer-html": "templates/contracts/footer.html",
#         },
#     )
#     response = HttpResponse(pdf, content_type="application/pdf;")
#     response["Content-Disposition"] = 'attachment; filename = "contract.pdf"'
#     return response


def contract_sign_date(doj):
    cal_Obj = CalendarLeaves.objects.all()
    for x in range(int(CalendarLeaves.objects.count())):
        temp = cal_Obj[x].date
        if (doj != temp) and (doj.weekday() != 5 and doj.weekday() != 6):
            pass
        else:
            doj = doj - timedelta(days=1)
            contract_sign_date(doj)
    return doj


def final_contract_pdf(request, con_id):
    contract = Contract.objects.get(id=con_id)
    sections_fr = ContractSection.objects.filter(
        contract=contract, language="fr"
    ).order_by("order")
    sections_en = ContractSection.objects.filter(
        contract=contract, language="en"
    ).order_by("order")
    doj = contract.from_date - timedelta(days=7)
    issue_date = contract_sign_date(doj)
    context = {
        "contract": contract,
        "sections_fr": sections_fr,
        "sections_en": sections_en,
        "issue_date": issue_date,
    }
    return render(request, "contracts/final_contract_pdf.html", context)


@login_required
def amend_contract(request):
    employee = IFPUser.objects.get(id=request.user.id)
    error_code = 0
    if request.method == "GET" and (request.user.is_superuser or employee.pi == True):
        form = AmendContractForm(proposer=request.user)
        return render(
            request,
            "contracts/amend_contract.html",
            {"form": form, "employee": employee},
        )
    elif request.method == "POST" and (
        request.user.is_superuser or employee.pi == True
    ):
        contract = Contract()
        form = AmendContractForm(request.POST, instance=contract)
        if form.is_valid():
            proposer = IFPUser.objects.get(id=request.user.id)
            employee_name = form.cleaned_data["amend_user"]
            old_con = Contract.objects.filter(
                first_name=employee_name.first_name, last_name=employee_name.last_name
            )
            new_con = old_con.last()
            new_con.pk = None
            new_con.id = None
            new_con._state.adding = True
            new_con.contract_type = None
            new_con.resubmit_reason = None
            new_con.amend = True
            new_con.proposer = proposer
            new_con.status = "Proposed"
            project_name = form.cleaned_data["project"]
            new_con.project = project_name
            budget = form.cleaned_data["budget"]
            new_con.budget = budget
            contract_from_date = form.cleaned_data["from_date"]
            new_con.from_date = contract_from_date
            contract_to_date = form.cleaned_data["to_date"]
            new_con.to_date = contract_to_date
            new_con.task = form.cleaned_data["task"]
            new_con.department = form.cleaned_data["department"]
            new_con.comment = form.cleaned_data["comment"]
            new_con.eligible_for_missions = form.cleaned_data["eligible_for_missions"]
            new_con.affiliated_to_institution = form.cleaned_data[
                "affiliated_to_institution"
            ]
            new_con.installment = form.cleaned_data["installment"]
            new_con.installment_type = form.cleaned_data["installment_type"]
            new_con.currency = form.cleaned_data["currency"]
            project = Project.objects.get(name=project_name)
            if budget > project.budget:
                error_code = 1
                return render(
                    request,
                    "contracts/new_contract_form.html",
                    {"form": form, "error_code": error_code, "employee": employee},
                )
            if project.to_date:
                if contract_to_date > project.to_date:
                    error_code = 2
                    return render(
                        request,
                        "contracts/new_contract_form.html",
                        {"form": form, "error_code": error_code, "employee": employee},
                    )
            if contract_from_date > contract_to_date:
                error_code = 3
                return render(
                    request,
                    "contracts/new_contract_form.html",
                    {"form": form, "error_code": error_code, "employee": employee},
                )
            new_con.save()
            full_name = str(
                IFPUser.objects.get(id=request.user.id).first_name
                + " "
                + IFPUser.objects.get(id=request.user.id).last_name
            )
            audit_trail = AuditTrail.objects.create(
                audit_status="Proposed",
                contract=new_con,
                changed_by=full_name,
                changed_at=datetime.now(),
            )
            audit_trail.save()
            config = Config.objects.all().last()
            contract_email_rep = [config.admin.email, config.gensec.email]
            send_mail_func.apply_async(
                kwargs={
                    "message": "New Amend contract is proposed by "
                    + str(new_con.proposer)
                    + " for "
                    + new_con.project.name
                    + " From "
                    + str(new_con.from_date)
                    + " till "
                    + str(new_con.to_date)
                    + " with budget of "
                    + str(new_con.budget)
                    + " is waiting for acknowledgement",
                    "subject": "Opera - Amend Contract Proposed",
                    "to_email": contract_email_rep,
                    "action_link": "https://opera.ifpindia.org/contracts/list",
                }
            )
            return redirect(reverse("contracts_list"))
