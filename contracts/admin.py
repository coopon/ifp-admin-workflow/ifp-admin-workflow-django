from django.contrib import admin
from contracts.models import Contract, ContractSection, ContractTemplate


class ContractAdmin(admin.ModelAdmin):
    pass


admin.site.register(Contract, ContractAdmin)




class ContractDetailsAdmin(admin.ModelAdmin):
    pass


admin.site.register(ContractTemplate)
admin.site.register(ContractSection)
