from django import forms
from contracts.models import ContractTemplate, SectionTemplate
from projects.models import Project
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from djrichtextfield.widgets import RichTextWidget

class ContractTemplateForm(forms.ModelForm):
    class Meta:
        model = ContractTemplate
        fields = ("type","en_title","fr_title", "get_bank_details", "get_dependent_for_insurance","get_educational_details")

        labels = {
            "type": "Contract Type",
            "en_title":"English Title",
            "fr_title":"French Title",
            "get_bank_details": "Get Bank Account details",
            "get_dependent_for_insurance": "Get Dependents for Insurance",
            "get_educational_details":"Get Educational Details",
        }

class ContractSectionTemplateForm(forms.ModelForm):
    description = forms.CharField(widget=RichTextWidget(attrs={"class": "textarea"}))
    class Meta:
        model = SectionTemplate
        fields = ("tittle",
            "description",
            "language",
            "order")

        labels = {
            "tittle": "Title",
            "description": "Description",
            "language": "Language",
            "order": "Order"
        }

