from django import forms
from django.core.validators import FileExtensionValidator
from contracts.models import Contract, ContractSection
from projects.models import Project
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from djrichtextfield.widgets import RichTextWidget
from common.utils import MaxSizeValidator


class NewContractForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.proposer = kwargs.pop("proposer", None)
        super().__init__(*args, **kwargs)
        self.projects = Project.objects.filter(principal_investigator=self.proposer)

    class Meta:
        model = Contract
        fields = (
            "first_name",
            "last_name",
            "father_name",
            "email",
            "nationality",
            "from_date",
            "to_date",
            "department",
            "project",
            "designation",
            "budget",
            "installment",
            "installment_type",
            "task",
            "comment",
            "eligible_for_missions",
            "affiliated_to_institution",
        )
        labels = {
            "first_name": "First Name (As per bank account)",
            "last_name": "Last Name (As per bank account)",
            "father_name":"Father's Name",
            "nationality": "Nationality",
            "from_date": "Contract from",
            "to_date": "Contract until",
            "project": "Project",
            "designation":"Designation",
            "budget": "Total Budget (INR)",
            "installment": "Installment Budget (INR)",
            "installment_type": "Installment Type",
            "task": "Task / deliverables",
            "comment": "Other Comments",
            "eligible_for_missions": "Eligible for missions?",
            "affiliated_to_institution": "Afilliated to any institution?",
        }
        widgets = {
            "from_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "to_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "budget": forms.NumberInput(attrs={"class": "input", "type": "number"}),
        }
class AmendContractForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.proposer = kwargs.pop("proposer", None)
        super().__init__(*args, **kwargs)
        self.projects = Project.objects.filter(principal_investigator=self.proposer)

    class Meta:
        model = Contract
        fields = (
            "amend_user",
            "from_date",
            "to_date",
            "department",
            "project",
            "designation",
            "budget",
            "installment",
            "installment_type",
            "task",
            "comment",
            "eligible_for_missions",
            "affiliated_to_institution",
        )
        labels = {
            "amend_user": "Select the Employee",
            "from_date": "Contract from",
            "to_date": "Contract until",
            "project": "Project",
            "designation":"Designation",
            "budget": "Total Budget (INR)",
            "installment": "Installment Budget (INR)",
            "installment_type": "Installment Type",
            "task": "Task / deliverables",
            "comment": "Other Comments",
            "eligible_for_missions": "Eligible for missions?",
            "affiliated_to_institution": "Afilliated to any institution?",
        }
        widgets = {
            "from_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "to_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "budget": forms.NumberInput(attrs={"class": "input", "type": "number"}),
        }

class UploadSignedCopy(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "contract_signed_copy",
        )
        labels = {
            "contract_signed_copy":"",
        }
        
# Onboard Forms
class OnBoardPersonalDetails(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "title",
            "recent_photo",
             "first_name",
            "last_name",
            "father_name",
            "date_of_birth",
            "gender",
            "marital_status",
            "phone_number",
            "current_address",
            "permanent_address",
            "permanent_address_proof",
            "cv",
            "signature_photo",
        )
        labels = {
            "title": "Title",
            "recent_photo": "Recent Photo",
            "first_name":"First Name (As per bank account)",
            "last_name":"Last Name (As per bank account)",
            "father_name":"Father Name",
            "gender": "Gender",
            "date_of_birth": "Date of Birth",
            "phone_number":"Phone Number",
            "marital_status": "Marital Status",
            "current_address": "Current Address",
            "permanent_address": "Permanent Address",
            "permanent_address_proof": "Permanent Address Proof",
            "cv": "Curriculam Vitae",
            "signature_photo": "Signature Photo",
        }
        widgets = {
            "date_of_birth": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }

class OnBoardDependentDetails(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "spouse_name",
            "spouse_dob",
            "spouse_relation",
            "spouse_id_proof",
            "child1_name",
            "child1_dob",
            "child1_relation",
            "child1_id_proof",
            "child2_name",
            "child2_dob",
            "child2_relation",
            "child2_id_proof",
        )
        labels = {
            "spouse_name": "Spouse Name",
            "spouse_dob": "Date of Birth",
            "spouse_relation": "Relationship",
            "spouse_id_proof":"ID Proof",
            "child1_name": "Child1 Name",
            "child1_dob": "Date of Birth",
            "child1_relation": "Relationship",
            "child1_id_proof":"ID Proof",
            "child2_name": "Child2 Name",
            "child2_dob": "Date of Birth",
            "child2_relation": "Relationship",
            "child2_id_proof":"ID Proof",
        }
        widgets = {
            "spouse_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "child1_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "child2_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }


class OnBoardEmergencyContact(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "emergency_contact_name",
            "emergency_contact_relationship",
            "emergency_contact_mobile",
        )
        labels = {
            "emergency_contact_name": "Name",
            "emergency_contact_relationship": "Relationship",
            "emergency_contact_mobile": "Mobile",
        }


class OnBoardAffiliatedInstitution(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "affiliated_institution",
            "affiliated_institution_dor",
            "affiliated_institution_email",
            "affiliated_institution_id",
        )
        labels = {
            "affiliated_institution": "Name of the Affiliated Institution",
            "affiliated_institution_dor": "Date of Registration in the Institution",
            "affiliated_institution_email": "Email provided by your Institution",
            "affiliated_institution_id": "Proof of Affiliation (ID / Letter of Affiliation)",
        }
        widgets = {
            "affiliated_institution_dor": forms.DateInput(
                attrs={"class": "input", "type": "date"}
            ),
        }
class OnBoardEducationDetails(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "institute_name",
            "degree_name",
            "year_of_passing",
            "mark_percent",
            "degree_certificate",
        )
        labels = {
            "institute_name": "Institute/University Name",
            "degree_name": "Degree/Course Name",
            "year_of_passing": "Year of Passing",
            "mark_percent": "Total Mark Percentage",
            "degree_certificate": "Upload Degree Certificate",
        }
        widgets = {
            "year_of_passing": forms.DateInput(
                attrs={"class": "input", "type": "date"}
            ),
        }


class OnBoardVisa(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "passport",
            "passport_name",
            "passport_number",
            "visa_number",
            "visa_start_date",
            "visa_end_date",
            "visa_file",
        )
        labels = {
            "passport": "Passport",
            "passport_name":"Name as in Passport",
            "passport_number":"Passport Number",
            "visa_number": "Number",
            "visa_start_date": "Start Date",
            "visa_end_date": "End Date",
            "visa_file": "VISA Copy",
        }
        widgets = {
            "visa_start_date": forms.DateInput(
                attrs={"class": "input", "type": "date"}
            ),
            "visa_end_date": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }


class OnBoardBankAccount(forms.ModelForm):
    class Meta:
        model = Contract
        fields = (
            "acc_name",
            "acc_no",
            "acc_type",
            "bank_name",
            "branch",
            "uid_type",
            "uid",
            "pan",
            "bank_details_proof",
        )
        labels = {
            "pan":"PAN Number",
            "acc_name": "Account holder's name",
            "acc_no": "Account Number",
            "acc_type": "Account Type",
            "bank_name": "Bank Name",
            "branch": "Bank Branch",
            "uid": "UID",
            "uid_type": "UID Type",
            "bank_details_proof": "Bank Passbook / Old Check Leaf Copy",
        }

#Contract Resubmit
class ContractResubmitForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ("resubmit_reason",)
        labels = {
            "resubmit_reason": "Comments",
        }


class ContractSectionForm(forms.ModelForm):
    description = forms.CharField(widget=RichTextWidget(attrs={"class": "textarea"}))

# Contract Forms
class SetContractTypeForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ("contract_type",)
        labels = {
            "contract_type": "Contract Type",
        }


class ContractSectionForm(forms.ModelForm):
    description = forms.CharField(widget=RichTextWidget(attrs={"class": "textarea"}))

    class Meta:
        model = ContractSection
        fields = ("tittle", "description", "language", "order")

        labels = {
            "tittle": "Title",
            "description": "Description",
            "language": "Language",
            "order": "Order",
        }

class ChronoNumberForm(forms.ModelForm):
    class Meta:
        model = Contract
        fields = ("chrono_no",)
        labels = {"chrono_no": "Chrono Number"}


