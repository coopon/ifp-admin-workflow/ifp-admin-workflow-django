from uuid import uuid4
from django.db import models
from django.core.validators import FileExtensionValidator
from accounts.models import IFPUser
from common.models import CURRENCY_CHOICES
from department.models import Department
from projects.models import Project
from common.utils import UploadToPathAndRename, MaxSizeValidator
from django_countries.fields import CountryField


class ContractTemplate(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    type = models.CharField(null=True, blank=True, max_length=255)
    get_bank_details = models.BooleanField(default=True)
    get_dependent_for_insurance = models.BooleanField(default=False)
    get_educational_details = models.BooleanField(default=False)
    en_title = models.CharField(null=True, blank=True, max_length=255)
    fr_title = models.CharField(null=True, blank=True, max_length=255)

    def __str__(self):
        return str(self.type)


class SectionTemplate(models.Model):
    CHOICES = (
        ("fr", "French"),
        ("en", "English"),
    )
    order = models.IntegerField(default=10)
    tittle = models.CharField(null=True, blank=True, max_length=255)
    description = models.TextField(null=True, blank=True)
    language = models.CharField(
        null=True, default="fr", max_length=255, choices=CHOICES
    )
    contract_template = models.ForeignKey(
        ContractTemplate, on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return str(self.tittle)


class Contract(models.Model):
    CONTRACT_STATUS_CHOICES = [
        ("Proposed", "Proposed"),  # Default status when a contract is proposed by PI
        (
            "Acknowledged",
            "Acknowledged",
        ),  # Generates a link to sent to concerned person and waits for the employee to fill in the details
        (
            "Employee Submitted",
            "Employee Submitted",
        ),  # Employee finalize the submitted information
        (
            "Employee Resubmit",
            "Employee Resubmit",
        ),  # If there is a flaw in the submitted information and email is sent to the employee
        (
            "Contract Drafting",
            "Contract Drafting",
        ),  # Goes to this phase after the admin validated the information
        (
            "Contract Redrafting",
            "Contract Redrafting",
        ),  # Comes here if needs change or update
        (
            "Contract Draft Review",
            "Contract Draft Review",
        ),  # Once the drafting is done and ready for review by SG
        ("Contract Drafted", "Contract Drafted"),  # SG Finalised the contract
        ("Signed", "Signed"),  # Physical signed docs to be scanned & uploaded
        ("Completed", "Completed"),
    ]
    MARITAL_STATUS_CHOICES = [
        ("married", "Married"),
        ("unmarried", "Unmarried"),
    ]
    INSTALLMENT_TYPE_CHOICES = [
        ("Weekly", "Weekly"),
        ("Two Weeks", "Two Weeks"),
        ("Monthly", "Monthly"),
        ("Date", "Date"),
        ("Task", "Task"),
    ]
    DESIGNATION_CHOICES = [
        ("Technical Agent", "Technical Agent"),
        ("Technical Assistant", "Technical Assitant"),
        ("Technician", "Technician"),
        ("Engineer Assistant", "Engineer Assistant"),
        ("Research Fellow - JR", "Research Fellow - JR"),
        ("Research Fellow - SR", "Research Fellow - SR"),
        ("Research Director", "Research Director"),
    ]
    amend = models.BooleanField(default=False)
    resubmit_reason = models.TextField(blank=True, null=True, default=None)
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    chrono_no = models.CharField(max_length=255, null=True, blank=True, default=None)
    from_date = models.DateField()
    to_date = models.DateField()
    contract_type = models.ForeignKey(
        ContractTemplate, on_delete=models.SET_NULL, null=True
    )
    amend_user = models.ForeignKey(
        IFPUser, on_delete=models.CASCADE, related_name="amend_user", null=True
    )
    proposer = models.ForeignKey(
        IFPUser, on_delete=models.CASCADE, blank=True, null=True
    )
    designation = models.CharField(
        max_length=100, blank=True, null=True, choices=DESIGNATION_CHOICES
    )
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=50, choices=CONTRACT_STATUS_CHOICES, default="Proposed"
    )
    task = models.TextField("Task / deliverables")
    comment = models.TextField("Other Comments", blank=True, null=True)
    eligible_for_missions = models.BooleanField(default=False)
    nationality = CountryField(blank=True, null=True, default="IN")
    budget = models.IntegerField(default=0)
    installment = models.IntegerField(default=0)
    installment_type = models.CharField(
        max_length=50, choices=INSTALLMENT_TYPE_CHOICES, default="Monthly"
    )
    currency = models.CharField(max_length=10, choices=CURRENCY_CHOICES)
    # Personal details
    TITLE_CHOICES = [("Mr.", "Mr."), ("Ms.", "Ms."), ("Mrs.", "Mrs.")]
    GENDER_CHOICES = [
        ("Male", "Male"),
        ("Female", "Female"),
        ("Transgender", "Transgender"),
    ]
    title = models.CharField(
        max_length=10, blank=True, null=True, choices=TITLE_CHOICES
    )
    gender = models.CharField(
        max_length=20, blank=True, null=True, choices=GENDER_CHOICES
    )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    father_name = models.CharField(max_length=255, null=True)

    marital_status = models.CharField(
        max_length=64, choices=MARITAL_STATUS_CHOICES, default="unmarried"
    )
    date_of_birth = models.DateField(blank=True, null=True)
    email = models.EmailField(max_length=255)
    phone_number = models.CharField(max_length=255)
    recent_photo = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "recent_photo_"),
        validators=[
            FileExtensionValidator(["jpg", "jpeg", "png"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    signature_photo = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "signature_photo_"),
        validators=[
            FileExtensionValidator(["jpg", "jpeg", "png"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    cv = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "cv_"),
        validators=[FileExtensionValidator(["pdf"]), MaxSizeValidator("10MB")],
        default=None,
    )
    pan = models.CharField(max_length=255)
    contract_signed_copy = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "contract_signature_copy_"),
        validators=[
            FileExtensionValidator(["jpg", "jpeg", "png", "pdf", "docx"]),
            MaxSizeValidator("10MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Dependent details
    spouse_name = models.CharField(max_length=255, null=True, blank=True)
    child1_name = models.CharField(max_length=255, null=True, blank=True)
    child2_name = models.CharField(max_length=255, null=True, blank=True)
    spouse_relation = models.CharField(max_length=255, null=True, blank=True)
    child1_relation = models.CharField(max_length=255, null=True, blank=True)
    child2_relation = models.CharField(max_length=255, null=True, blank=True)
    spouse_dob = models.DateField(blank=True, null=True)
    child1_dob = models.DateField(blank=True, null=True)
    child2_dob = models.DateField(blank=True, null=True)
    spouse_id_proof = bank_details_proof = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "spouse_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    child1_id_proof = bank_details_proof = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "child1_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    child2_id_proof = bank_details_proof = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "child2_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Addresses
    current_address = models.TextField()
    permanent_address = models.TextField(null=True, blank=True)
    permanent_address_proof = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "permanent_address_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        default=None,
    )
    # Emergency Contact
    emergency_contact_name = models.CharField(null=True, blank=True, max_length=255)
    emergency_contact_relationship = models.CharField(
        null=True, blank=True, max_length=255
    )
    emergency_contact_mobile = models.CharField(null=True, blank=True, max_length=255)
    # Education details
    institute_name = models.CharField(null=True, blank=True, max_length=255)
    degree_name = models.CharField(null=True, blank=True, max_length=255)
    year_of_passing = models.DateField(null=True, blank=True)
    mark_percent = models.IntegerField(null=True, blank=True)
    degree_certificate = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "degree_certificate_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Affiliated Institution details
    affiliated_to_institution = models.BooleanField(default=False)
    affiliated_institution = models.CharField(null=True, blank=True, max_length=255)
    affiliated_institution_dor = models.DateField(
        null=True, blank=True, max_length=255, default=None
    )
    affiliated_institution_email = models.EmailField(
        null=True, blank=True, max_length=255
    )
    affiliated_institution_id = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "affiliated_institution_id_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Visa Details
    passport = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "passport_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    passport_name = models.CharField(max_length=100, blank=True, null=True)
    passport_number = models.CharField(max_length=100, blank=True, null=True)
    visa_number = models.CharField(null=True, blank=True, max_length=255)
    visa_start_date = models.DateField(blank=True, null=True)
    visa_end_date = models.DateField(blank=True, null=True)
    visa_file = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "visa_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Bank Details
    CURRENT_ACC = "CUR"
    SAVINGS_ACC = "SB"
    SWIFT = "SWIFT"
    IBAN = "IBAN"
    IFSC = "IFSC"
    ACC_TYPE_CHOICES = [
        (CURRENT_ACC, "Current Account"),
        (SAVINGS_ACC, "Savings Account"),
    ]
    UID_TYPE_CHOICES = [
        (IBAN, "IBAN"),
        (IFSC, "IFSC"),
        (SWIFT, "SWIFT"),
    ]
    acc_name = models.CharField(max_length=100)
    acc_no = models.CharField(max_length=40)
    branch = models.CharField(max_length=100, blank=True, null=True)
    uid = models.CharField(max_length=40, blank=True, null=True)
    uid_type = models.CharField(
        max_length=10, blank=True, null=True, choices=UID_TYPE_CHOICES
    )
    acc_type = models.CharField(
        max_length=10, blank=True, null=True, choices=ACC_TYPE_CHOICES
    )
    bank_name = models.CharField(max_length=100, blank=True, null=True)
    bank_details_proof = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "bank_details_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        default=None,
    )

    def __str__(self):
        if (
            self.first_name is not None
            and self.last_name
            and self.email is not None
            and self.proposer.first_name is not None
        ):
            return str(
                str(self.first_name + " " + self.last_name)
                + " | "
                + str(self.email)
                + " | "
                + str(self.proposer.first_name)
            )
        else:
            return str(self.email)


class AuditTrail(models.Model):
    contract = models.ForeignKey(
        Contract, on_delete=models.CASCADE, related_name="audit_trail"
    )
    audit_status = models.CharField(max_length=50, blank=True)
    changed_by = models.CharField(max_length=255, blank=True)
    changed_at = models.DateField(blank=True)


class ContractSection(models.Model):
    CHOICES = (
        ("fr", "French"),
        ("en", "English"),
    )
    order = models.IntegerField(default=10)
    tittle = models.CharField(null=True, blank=True, max_length=255)
    description = models.TextField(null=True, blank=True)
    language = models.CharField(
        null=True, default="fr", max_length=255, choices=CHOICES
    )
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.tittle)


class ContractHistory(models.Model):
    contract = models.ForeignKey(Contract, on_delete=models.CASCADE)
    employee = models.ForeignKey(IFPUser, on_delete=models.CASCADE)

    def __str__(self):
        return (
            str(self.contract.contract_type)
            + " | "
            + str(self.contract.from_date)
            + " - "
            + str(self.contract.to_date)
        )
