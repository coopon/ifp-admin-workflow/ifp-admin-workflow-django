from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_list_or_404, get_object_or_404
from django.views.decorators.http import require_http_methods

from accounts.models import IFPUser
from projects.models import Project
from projects.forms import ProjectForm


@login_required
def project_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    projects = Project.objects.all()
    return render(request, "projects/home.html", {"projects": projects,"employee":employee})


@login_required
def project_create(request):
    employee = IFPUser.objects.get(id=request.user.id)
    projects = Project.objects.all()
    project_creation_form = ProjectForm()
    if request.method == "POST" and request.user.is_superuser:
        project_creation_form = ProjectForm(request.POST,request.FILES)
        if project_creation_form.is_valid():
            a = project_creation_form.save()
            project_creation_form = ProjectForm()
            return redirect(reverse("project_list"))
    else:
        return render(
            request,
            "projects/create_project.html",
            {"projects": projects, "project_creation_form": project_creation_form,"employee":employee},
        )


@login_required
def project_update(request, id=None):
    project_creation_form = None
    project = get_object_or_404(Project, id=id)
    if request.method == "POST" and request.user.is_superuser:
        project_creation_form = ProjectForm(request.POST,request.FILES, instance=project)
        if project_creation_form.is_valid():
            a = project_creation_form.save()
            project_creation_form = ProjectForm()
            return redirect(reverse("project_list"))
    else:
        project_creation_form = ProjectForm(instance=project)
        return render(
            request,
            "projects/project_edit.html",
            {"project_creation_form": project_creation_form, "project": project},
        )


@login_required
def project_view(request, id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    project = get_object_or_404(Project, id=id)
    return render(
        request,
        "projects/project.html",
        {"project": project,"employee":employee},
    )

@login_required
def project_delete(request, id=None):
    if id:
        project = get_object_or_404(Project, id=id)
        if request.user.is_superuser:
            project.delete()
    return redirect(reverse("project_list"))