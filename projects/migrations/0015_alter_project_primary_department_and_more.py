# Generated by Django 5.0 on 2024-08-19 17:48

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("department", "0001_initial"),
        ("projects", "0014_project_description"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="primary_department",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="department_primary",
                to="department.department",
                verbose_name="Primary Department",
            ),
        ),
        migrations.AlterField(
            model_name="project",
            name="secondary_departments",
            field=models.ManyToManyField(
                blank=True,
                null=True,
                related_name="department_secondary",
                to="department.department",
                verbose_name="Supporting Departments",
            ),
        ),
    ]
