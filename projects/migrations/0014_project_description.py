# Generated by Django 4.2 on 2023-06-01 14:14

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0013_alter_project_from_date_alter_project_to_date"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="description",
            field=models.TextField(
                default=django.utils.timezone.now, verbose_name="Project Description"
            ),
            preserve_default=False,
        ),
    ]
