# Generated by Django 4.1 on 2022-08-17 05:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("projects", "0005_project_principal_investigator_alter_project_members"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="display_picture",
            field=models.ImageField(
                default="defaults/user.png", upload_to="projects/display_picture/"
            ),
        ),
    ]
