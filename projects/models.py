from django.db import models
from django.utils.translation import gettext as _
from accounts.models import IFPUser
from common.models import CURRENCY_CHOICES


class Project(models.Model):
    project_status_choices = (
        ("started", "Started"),
        ("ongoing", "On-Going"),
        ("done", "Completed"),
    )

    name = models.CharField(_("Project name"), max_length=500)
    description = models.TextField(_("Project Description"))
    status = models.CharField(
        _("Project status"), max_length=50, choices=project_status_choices
    )
    primary_department = models.ForeignKey(
        "department.Department",
        verbose_name=_("Primary Department"),
        on_delete=models.CASCADE,
        related_name="department_primary",
    )
    secondary_departments = models.ManyToManyField(
        "department.Department",
        verbose_name=_("Supporting Departments"),
        related_name="department_secondary",
        blank=True,
        null=True,
    )
    members = models.ManyToManyField(
        "accounts.IFPUser",
        verbose_name=_("Members of the project"),
        related_name="project_members",
    )
    from_date = models.DateField()
    to_date = models.DateField()
    principal_investigator = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="project_pi",
    )
    display_picture = models.ImageField(
        default="defaults/work.jpg", upload_to="projects/display_picture/"
    )
    budget = models.IntegerField(_("Budget"), default=0, blank=True, null=True)
    budget_currency = models.CharField(
        choices=CURRENCY_CHOICES, default="INR", max_length=8, null=True, blank=True
    )

    class Meta:
        verbose_name = _("Projects")
        verbose_name_plural = _("Projects")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Projects_detail", kwargs={"pk": self.pk})
