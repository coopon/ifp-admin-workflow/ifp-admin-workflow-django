from django import forms
from projects.models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        widgets = {
            "to_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "from_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
        }
        exclude = ("",)
