from django.urls import path, re_path

from projects.views import project_list, project_create, project_update, project_view, project_delete

urlpatterns = [
    path("", project_list, name="project_list"),
    path("create/", project_create, name="project_create"),
    path("update/<int:id>/", project_update, name="project_update"),
    path("delete/<int:id>", project_delete, name="project_delete"),
    path("project/<int:id>/", project_view, name="project_view"),
]
