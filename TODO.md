# TODO

# Notifications

- administration
- finance
- it department

# People / Employees

- Filters
- Export CSV
  - Export only filtered data
- Expire PII after 3 years

# Contracts
- Propose contract - date format - dd/mm/yyyy
- Propose contract - integer - No negative

- Onboarding link sent to proposed should be secured
- Onboarding - UID type above UID - Done

- Contract header in PDF
- Contract signature through upload signature
- Signature process - prospective, PI, HOD, Director
- Contract offboarding

## Types

- Fellowship
- CDD, CDI - insurance

## Fields

# Mail

# Timeline

- Contracts - Finish TODO
- Data upload -
- Notifications -
- Missions -

# Missions
- Email template
- Project- forex, sub project from contract and missions
- PI Normal view in contract
- responsive image
- Hardcode mission status as permission
- Refactor home page
- Add Mission Proposer to members if members is empty
- Mission members is not storing
- Propose - Number to be manually filled
- Propose

- Propose -> Generate Mission Order -> Finish Mission -> Travel Date Again -> Claims Form -> Reimbursement 

## Notifications

- Contract finish - IT Department, Library
- Password reset email - change text

# Diagrams

- New staff onboarding
- Existing staff onboarding
- Missions onboarding

# Leaves
- Email date format to dd-mm-yyyy

# Auth

- Reset email mail from site to be changed in PROD

# Admin Dashboard

- DAUs, WAUs
- Metrics for Leaves, Contracts, Projects, employee

# Emails
- Template - remove email ID
- Leaves - missions@ifpindia.org
- Contracts - sg & administration@ifpindia.org

# Projects
- Change member input method
- Multiple PIs
