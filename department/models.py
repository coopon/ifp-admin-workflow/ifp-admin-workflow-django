from django.utils.translation import gettext as _
from django.db import models
from django.urls import reverse


class Department(models.Model):
    id = models.CharField(
        _("Department Id"),
        max_length=50,
        primary_key=True,
        unique=True,
        null=False,
        blank=False,
    )
    name = models.CharField(_("Department Name"), max_length=200)
    description = models.TextField(_("Department Description"))
    department_members = models.ManyToManyField(
        "accounts.IFPUser",
        verbose_name=_("Members of the department"),
        related_name="department",
    )
    display_picture = models.ImageField(
        default="defaults/work.jpg", upload_to="departments/display_picture/"
    )

    class Meta:
        verbose_name = _("Department")
        verbose_name_plural = _("Departments")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Department_detail", kwargs={"pk": self.pk})
