from ninja import Router
from .models import Department
from ninja.security import django_auth
from ninja import Schema
from typing import Optional
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

router = Router()


class DepartmentSchema(Schema):
    id: str
    name: str
    description: str


class DepartmentPutSchema(Schema):
    name: str
    description: str


class DepartmentDeleteSchema(Schema):
    id: str


@router.get("/")
def list_departments(request):
    return [
        {
            "id": e.id,
            "name": e.name,
            "description": e.description,
            "display_picture": e.display_picture.url,
            "members": [
                {"name": f.get_full_name(), "id": f.emp_id}
                for f in e.department_members.all()
            ],
        }
        for e in Department.objects.all()
    ]


@router.post("/create")
def dept_create(request, payload: DepartmentSchema):
    dept = Department.objects.create(**payload.dict())
    return dept


@router.get("/{dept_id}")
def dept_get_details(request, dept_id: str):
    dept = Department.objects.get(id=dept_id)
    dept_members = [
        i
        for i in dept.department_members.all().values(
            "first_name", "last_name", "email"
        )
    ]
    return {"name": dept.name, "description": dept.description, "members": dept_members}


@router.put("/{dept_id}")
def dept_put_details(request, dept_id: str, payload: DepartmentPutSchema):
    dept = get_object_or_404(Department, id=dept_id)
    for attr, value in payload.dict().items():
        setattr(dept, attr, value)
    dept.save()
    return dept


@router.delete("/{dept_id}")
def dept_delete(request, dept_id: str):
    dept = Department.objects.get(id=dept_id)
    dept.delete()
    return {"success": True}
