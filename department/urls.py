from django.urls import path
from department.views import (
    add_department,
    department_view,
    department_update,
    department_list,
    department_delete,
)

urlpatterns = [
    path("department/create", add_department, name="add_department"),
    path("department/update/<str:id>/", department_update, name="department_update"),
    path("delete/<str:id>", department_delete, name="department_delete"),
    path("department/<str:id>/", department_view, name="department_view"),
    path("department/", department_list, name="department_list"),
]
