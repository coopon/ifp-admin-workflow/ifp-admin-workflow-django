# from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods, require_GET
from department.forms import DepartmentForm
from accounts.models import IFPUser
from django.shortcuts import render, redirect, reverse
from department.models import Department


@login_required
@require_http_methods(["GET", "POST"])
def add_department(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST" and request.user.is_superuser:
        department_form = DepartmentForm(request.POST, request.FILES)
        if department_form.is_valid():
            department_form.save()
        return redirect(reverse("department_list"))
    else:
        department_form = DepartmentForm()
        departments = Department.objects.all()
        return render(
            request,
            "common/department_new.html",
            {
                "departments": departments,
                "department_form": department_form,
                "employee": employee,
            },
        )


@login_required
@require_GET
def department_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    departments = Department.objects.all()
    return render(
        request,
        "common/department_list.html",
        {"departments": departments, "employee": employee},
    )


@login_required
@require_http_methods(["GET", "POST"])
def department_update(request, id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    department_form = None
    department = get_object_or_404(Department, id=id)
    if request.method == "POST" and request.user.is_superuser:
        department_form = DepartmentForm(
            request.POST, request.FILES, instance=department
        )
        if department_form.is_valid():
            a = department_form.save()
            department_form = DepartmentForm()
            return redirect(reverse("department_list"))
    else:
        department_form = DepartmentForm(instance=department)
        return render(
            request,
            "common/department_edit.html",
            {
                "department_form": department_form,
                "department": department,
                "employee": employee,
            },
        )


@login_required
@require_GET
def department_delete(request, id=None):
    if id:
        department = get_object_or_404(Department, id=id)
        if request.user.is_superuser:
            department.delete()
    return redirect(reverse("department_list"))


@login_required
@require_GET
def department_view(request, id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    department = Department.objects.get(id=id)
    return render(
        request,
        "common/department_view.html",
        {"department": department, "employee": employee},
    )
