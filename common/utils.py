import os
from uuid import uuid4
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from django.db.models import FileField
from django.forms import forms
from django.template.defaultfilters import filesizeformat
from django.utils.translation import gettext as _
from django.utils.deconstruct import deconstructible
from django.conf import settings


def email_body_render(template, context={}):
    html = get_template("email/" + template)
    return html.render(context)


def email_body_action(subject, action_link, action_text, reject_reason, first_name):
    context = {
        "subject": subject,
        "action_link": action_link,
        "action_text": action_text,
        "reject_reason": reject_reason,
        "first_name": first_name,
    }
    text = email_body_render("email_action.txt", context)
    html = email_body_render("email_action.html", context)
    return {"html": html, "text": text}


def email_body_ifp_email_expiry(subject, expiring_emails, expired_emails):
    context = {
        "subject": subject,
        "expiring_emails": expiring_emails,
        "expired_emails": expired_emails,
    }
    text = email_body_render("email_ifp_email_expiry.txt", context)
    html = email_body_render("email_ifp_email_expiry.html", context)
    return {"html": html, "text": text}


@deconstructible
class UploadToPathAndRename:

    def __init__(self, path, prefix):
        self.path = path
        self.prefix = prefix

    def __call__(self, instance, filename):
        ext = filename.split(".")[-1]
        # get filename
        if instance.pk:
            filename = "{}{}.{}".format(self.prefix, instance.pk, ext)
        else:
            # set filename as random string
            filename = "{}{}.{}".format(self.prefix, uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.path, filename)


@deconstructible
class MaxSizeValidator:
    message = _(
        "Uploaded file size has exceeded the limit. Current File Size: “%(file_size)s” | "
        "Please keep the filesize under “%(max_file_size)s”"
    )
    code = "invalid_file_size"

    def __init__(self, size=10240, message=None, code=None):
        """
        * size - a number indicating the maximum file size allowed for upload.
        Works with the list of prefix ['KB','MB','GB','TB','PB','EB','ZB','YB']
        """
        self.size = size
        if message is not None:
            self.message = message
        if code is not None:
            self.code = code

    def bytesize(self):
        SUFFIXES = ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
        return int(float(self.size[:-2]) * 1024 ** (SUFFIXES.index(self.size[-2:]) + 1))

    def __call__(self, instance):
        if instance.file.size > self.bytesize():
            raise forms.ValidationError(
                self.message,
                code=self.code,
                params={
                    "max_file_size": self.size,
                    "file_size": filesizeformat(instance.file.size),
                },
            )

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__)
            and self.size == other.size
            and self.message == other.message
            and self.code == other.code
        )


class ContentTypeSizeRestrictedFileField(FileField):
    def __init__(self, *args, **kwargs):
        self.content_types = kwargs.pop("content_types", [])
        self.max_upload_size = kwargs.pop("max_upload_size", "")

        super(ContentTypeSizeRestrictedFileField, self).__init__(*args, **kwargs)


def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    sUrl = settings.STATIC_URL
    sRoot = settings.STATIC_ROOT
    mUrl = settings.MEDIA_URL
    mRoot = settings.MEDIA_ROOT

    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri

    # making sure that file exists
    if not os.path.isfile(path):
        raise Exception("media URI must start with %s or %s" % (sUrl, mUrl))
    return path
