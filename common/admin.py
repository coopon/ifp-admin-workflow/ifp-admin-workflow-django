from django.contrib import admin
from common.models import Config, CalendarLeaves


class ConfigAdmin(admin.ModelAdmin):
    pass


admin.site.register(Config, ConfigAdmin)

admin.site.register(CalendarLeaves)
