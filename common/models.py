from django.db import models
from accounts.models import IFPUser

CURRENCY_CHOICES = [
    ("INR", "Indian Rupee"),
    ("EUR", "Euro"),
]


class Config(models.Model):
    director = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_director",
    )
    gensec = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_gensec",
    )
    mission = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_mission",
    )
    admin = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_admin",
    )
    it = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_it",
    )
    library = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_library",
    )
    finance = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name="ifp_finance",
    )


class CalendarLeaves(models.Model):
    event = models.CharField(max_length=100)
    evenement = models.CharField(max_length=100, default="")
    date = models.DateField()

    def __str__(self):
        return self.event
