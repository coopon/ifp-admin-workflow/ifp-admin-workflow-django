# from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods, require_GET
from django.core import mail
from common.models import CalendarLeaves
from common.forms import HolidayForm
from django.http import HttpResponse
import csv
from accounts.models import IFPUser
from django.shortcuts import render, redirect, reverse
from accounts.tasks import send_mail_func


@login_required
@require_GET
def calendar_view(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET":
        leaves = CalendarLeaves.objects.all()
        return render(
            request,
            "common/calendar_view.html",
            {"leaves": leaves, "employee": employee},
        )


@login_required
@require_http_methods(["GET", "POST"])
def add_holiday(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST" and request.user.is_superuser:
        holiday_form = HolidayForm(request.POST)
        if holiday_form.is_valid():
            holiday_form.save()
        return redirect(reverse("calendar_view"))
    else:
        holiday_form = HolidayForm()
        return render(
            request,
            "common/holiday_new.html",
            {"holiday_form": holiday_form, "employee": employee},
        )


@login_required
@require_http_methods(["GET", "POST"])
def edit_holiday(request, cal_id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    cal_leave = CalendarLeaves.objects.get(id=cal_id)
    if request.method == "GET" and request.user.is_superuser:
        form = HolidayForm(instance=cal_leave)
        return render(
            request,
            "common/holiday_new.html",
            {"holiday_form": form, "employee": employee},
        )
    else:
        form = HolidayForm(request.POST, instance=cal_leave)
        if form.is_valid():
            form.save()
            return redirect(reverse("calendar_view"))


@login_required
@require_GET
def delete_holiday(request, cal_id=None):
    cal_leave = CalendarLeaves.objects.get(id=cal_id)
    if request.user.is_superuser:
        cal_leave.delete()
        return redirect(reverse("calendar_view"))


def send_mail(to, subject, body):
    with mail.get_connection() as connection:
        mail.EmailMessage(
            subject=subject,
            body=body,
            to=[to],
            connection=connection,
        ).send()


@login_required
@require_GET
def test_mail(request):
    send_mail_func.apply_async(
        kwargs={
            "message": "Testing",
            "subject": "Opera - Contract proposed",
            "to_email": ["noordine62@gmail.com"],
        }
    )
    return HttpResponse("<h1>Mail sent</h1>")


@require_GET
def staff_import(request):
    a = request.FILES["upload"].read().decode().splitlines()
    for row in a:
        #        print(row)
        spamreader = csv.reader(row, delimiter=",", skipinitialspace=True)
        row = row.split(",")
        first_name = row[0].strip()
        last_name = row[1].strip()
        designation = row[2].strip()
        email = row[3].strip()
        mail_date = row[4].strip()
        mail_expiry = (
            datetime.strptime(mail_date, "%Y-%m-%d")
            if mail_date != ""
            else datetime.today().date()
        )
        try:
            user = IFPUser.objects.get(email=email)
            user.first_name = first_name
            user.last_name = last_name
            user.mail_expiry = mail_expiry
            user.emp_id = row[5]
            user.save()
        except IFPUser.DoesNotExist:
            user = IFPUser(
                first_name=first_name,
                last_name=last_name,
                email=email,
                designation=designation,
                mail_expiry=mail_expiry,
                emp_id=row[5],
            )
            user.save()
    #        print(first_name, last_name, designation, email, mail_expiry)
    return HttpResponse("Import successful")


@require_GET
def ml_import(request):
    return HttpResponse("")


@require_GET
def dept_import(request):
    a = request.FILES["upload"].read().decode().splitlines()
    for row in a:
        #        print(row)
        spamreader = csv.reader(
            row, delimiter=",", quotechar='"', skipinitialspace=True
        )
        row = row.split(",")
        print(row)
        first_name = row[0].strip()
        last_name = row[1].strip()
        designation = row[2].strip()
        email = row[3].strip()
        mail_date = row[4].strip()
        mail_expiry = (
            datetime.strptime(mail_date, "%Y-%m-%d")
            if mail_date != ""
            else datetime.today().date()
        )
        try:
            user = IFPUser.objects.get(email=email)
        except IFPUser.DoesNotExist:
            user = IFPUser(
                first_name=first_name,
                last_name=last_name,
                email=email,
                designation=designation,
                mail_expiry=mail_expiry,
                emp_id=row[5],
            )
            user.save()
        print(first_name, last_name, designation, email, mail_expiry)
    return HttpResponse("Import successful")


@require_GET
def projects_import(request):
    return HttpResponse("")


@login_required
@require_GET
def dashboard_view(request):
    return HttpResponse("Dashboard coming soon")
