from django import forms
from common.models import CalendarLeaves


class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True


class HolidayForm(forms.ModelForm):
    class Meta:
        model = CalendarLeaves
        fields = ("event", "evenement", "date")
        labels = {
            "event": "Event",
            "evenement": "Événement",
            "date": "Date",
        }
        widgets = {
            "date": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }
