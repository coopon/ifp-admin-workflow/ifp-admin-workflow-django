from django.urls import path
from common.views import (
    test_mail,
    staff_import,
    dept_import,
    projects_import,
    dashboard_view,
    calendar_view,
    add_holiday,
    edit_holiday,
    delete_holiday,
)

urlpatterns = [
    path("test_mail", test_mail, name="test_mail"),
    path("staff_import", staff_import, name="staff_import"),
    path("dept_import", dept_import, name="dept_import"),
    path("projects_import", projects_import, name="projects_import"),
    path("dashboard", dashboard_view, name="dashboard_view"),
    path("calendar", calendar_view, name="calendar_view"),
    path("holiday_new", add_holiday, name="holiday_new"),
    path("calendar/<str:cal_id>/edit", edit_holiday, name="edit_holiday"),
    path("calendar/<str:cal_id>/delete", delete_holiday, name="delete_holiday"),
]
