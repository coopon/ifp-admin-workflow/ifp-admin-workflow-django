from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import IFPUser, BankAccount, Leave, MailingList, LeaveType


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = IFPUser
    list_display = [
        "email",
        "emp_id",
        "dob",
        "doj",
        "nationality",
    ]


# admin.site.register(IFPUser, CustomUserAdmin)


class BankAccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(BankAccount, BankAccountAdmin)

admin.site.register(Leave)

admin.site.register(LeaveType)

admin.site.register(IFPUser)

admin.site.register(MailingList)
