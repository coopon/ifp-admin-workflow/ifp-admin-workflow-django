from django.core.mail import send_mail
from accounts.models import IFPUser 
from django_project import settings
from common.utils import email_body_ifp_email_expiry,email_body_action
from datetime import datetime, timedelta
from celery import shared_task
from django_project import settings


@shared_task(name="IFP email expiring notification - IT Support")
def ifp_email_expiry_notify_mail():
    today = datetime.today()
    email_expiring_users = IFPUser.objects.filter(
        email__contains = "@ifpindia.org",
        mail_expiry__range = [today.strftime("%Y-%m-%d"), (today + timedelta(7)).strftime("%Y-%m-%d")]
        ).all()
    email_expired_users = IFPUser.objects.filter(
        email__contains = "@ifpindia.org",
        mail_expiry__lt = today.strftime("%Y-%m-%d")
        ).all()
    email_body = email_body_ifp_email_expiry("Please take necessary action in OPERA Portal", email_expiring_users, email_expired_users)
    # Send email function comes here
    if email_expiring_users or email_expired_users:
        send_mail(
            subject = "List of expiring IFP emails",
            from_email = settings.DEFAULT_FROM_EMAIL,
            recipient_list = ["itsupport@ifpindia.org"],
            message = email_body.get("text"),
            html_message = email_body.get("html")
            )
    return "Done"


@shared_task(name="send_mail_func")
def send_mail_func(message,subject,to_email,action_link ="opera.ifpindia.org", action_text="Click Here For More Details",reject_reason=None,first_name='Admin',*args, **kwargs):
    email_body = email_body_action(message, action_link, action_text,reject_reason, first_name)
    send_mail(
        subject= subject,
        message=email_body.get("text"),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to_email,
        fail_silently=True,
        html_message=email_body.get("html")
    )
    return "Done"

@shared_task(name="leave_carryover")
def leave_carryover():
    employees = IFPUser.objects.all()
    for employee in employees:
        if employee.casual_leaves >= 10:
            employee.casual_leaves = 30 + 10
        else:
            employee.casual_leaves = employee.casual_leaves + 30
        employee.default_casual_leaves = employee.casual_leaves
        employee.medical_leaves = 40
        employee.default_medical_leaves = employee.medical_leaves
        employee.compensation_leaves = 10
        employee.exceptional_leaves = 10
        employee.save()
    return "Leave CarryOver - Done"