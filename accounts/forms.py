from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import IFPUser


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = IFPUser
        fields = ("email", "username", "nationality")


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = IFPUser
        fields = ("email", "username", "nationality")


