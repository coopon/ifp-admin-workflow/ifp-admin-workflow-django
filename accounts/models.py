from django.utils.translation import gettext as _
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django_countries.fields import CountryField
from datetime import timedelta, datetime
from common.utils import UploadToPathAndRename, MaxSizeValidator
from django.core.validators import FileExtensionValidator


class BankAccount(models.Model):
    ACC_TYPE_CHOICES = [
        ("CUR", "Current Account"),
        ("SB", "Savings Account"),
    ]
    UID_TYPE_CHOICES = [
        ("IBAN", "IBAN"),
        ("IFSC", "IFSC"),
        ("SWIFT", "SWIFT"),
    ]
    acc_name = models.CharField(max_length=100, blank=True, null=True)
    acc_no = models.CharField(max_length=40, blank=True, null=True)
    branch = models.CharField(max_length=100, blank=True, null=True)
    uid = models.CharField(max_length=40, blank=True, null=True)
    uid_type = models.CharField(max_length=10, choices=UID_TYPE_CHOICES, default="IFSC")
    acc_type = models.CharField(max_length=10, choices=ACC_TYPE_CHOICES, default="SB")
    is_active = models.BooleanField(_("Active"), default=True)
    bank_name = models.CharField(max_length=100, blank=True, null=True)
    pan = models.CharField(null=True, blank=True, max_length=255)
    bank_details_proof = models.FileField(
        upload_to=UploadToPathAndRename("user/bank_proof/", "bank_details_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg", "png"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )

    def __str__(self):
        if self.acc_name is not None:
            return self.acc_name
        else:
            return "None"


class IFPUser(AbstractUser):
    TITLE_CHOICES = [("Mr.", "Mr."), ("Ms.", "Ms."), ("Mrs.", "Mrs.")]
    GENDER_CHOICES = [
        ("Male", "Male"),
        ("Female", "Female"),
        ("Transgender", "Transgender"),
    ]
    DESIGNATION_CHOICES = [
        ("Technical Agent", "Technical Agent"),
        ("Technical Assistant", "Technical Assitant"),
        ("Technician", "Technician"),
        ("Engineer Assistant", "Engineer Assistant"),
        ("Research Fellow - JR", "Research Fellow - JR"),
        ("Research Fellow - SR", "Research Fellow - SR"),
        ("Research Director", "Research Director"),
    ]
    EMPLOYEE_TYPE_CHOICES = [
        ("Student", "Student"),
        ("Expat", "Expat"),
        ("Indian", "Indian"),
    ]
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    father_name = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(max_length=50)
    emp_id = models.CharField(max_length=20)
    display_picture = models.ImageField(
        default="defaults/user.png", upload_to="users/display_picture/"
    )
    dob = models.DateField(blank=True, null=True)
    doj = models.DateField(blank=True, null=True)
    phone = models.CharField(max_length=25, blank=True, null=True)
    perm_addr = models.CharField(max_length=255, blank=True, null=True)
    comm_addr = models.CharField(max_length=255, blank=True, null=True)
    new = models.BooleanField(default=True)
    permanent_address_proof = models.FileField(
        upload_to=UploadToPathAndRename("user/address_proof/", "permanent_address_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    contract_from = models.DateField(blank=True, null=True)
    contract_to = models.DateField(blank=True, null=True)
    contract_type = models.CharField(max_length=50, blank=True, null=True)
    bank_accounts = models.ManyToManyField(
        "accounts.BankAccount",
        verbose_name=_("Bank Accounts associated"),
        blank=True,
        null=True,
    )
    user_note = models.CharField(_("User note"), max_length=50, blank=True, null=True)
    nationality = CountryField(blank=True, null=True, default="IN")
    casual_leaves = models.DecimalField(max_digits=5, decimal_places=1, default=30.0)
    exceptional_leaves = models.DecimalField(
        max_digits=5, decimal_places=1, default=10.0
    )
    compensation_leaves = models.DecimalField(
        max_digits=5, decimal_places=1, default=10.0
    )
    medical_leaves = models.DecimalField(max_digits=5, decimal_places=1, default=40.0)
    default_casual_leaves = models.DecimalField(
        max_digits=5, decimal_places=1, default=30.0
    )
    default_medical_leaves = models.DecimalField(
        max_digits=5, decimal_places=1, default=40.0
    )
    title = models.CharField(
        max_length=10, blank=True, null=True, choices=TITLE_CHOICES
    )
    gender = models.CharField(
        max_length=20, blank=True, null=True, choices=GENDER_CHOICES
    )
    designation = models.CharField(
        max_length=100, blank=True, null=True, choices=DESIGNATION_CHOICES
    )
    # Dependent details
    get_dependent_for_insurance = models.BooleanField(default=False)
    spouse_name = models.CharField(max_length=255, null=True, blank=True)
    child1_name = models.CharField(max_length=255, null=True, blank=True)
    child2_name = models.CharField(max_length=255, null=True, blank=True)
    spouse_relation = models.CharField(max_length=255, null=True, blank=True)
    child1_relation = models.CharField(max_length=255, null=True, blank=True)
    child2_relation = models.CharField(max_length=255, null=True, blank=True)
    spouse_dob = models.DateField(blank=True, null=True)
    child1_dob = models.DateField(blank=True, null=True)
    child2_dob = models.DateField(blank=True, null=True)
    spouse_id_proof = models.FileField(
        upload_to=UploadToPathAndRename("user/", "spouse_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    child1_id_proof = models.FileField(
        upload_to=UploadToPathAndRename("user/", "child1_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    child2_id_proof = models.FileField(
        upload_to=UploadToPathAndRename("user/", "child2_id_proof_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Education details
    get_educational_details = models.BooleanField(default=False)
    institute_name = models.CharField(null=True, blank=True, max_length=255)
    degree_name = models.CharField(null=True, blank=True, max_length=255)
    year_of_passing = models.DateField(null=True, blank=True)
    mark_percent = models.IntegerField(null=True, blank=True)
    degree_certificate = models.FileField(
        upload_to=UploadToPathAndRename("user/", "degree_certificate_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    get_bank_details = models.BooleanField(default=True)
    pi = models.BooleanField(default=False)
    # Visa Details
    passport = models.FileField(
        upload_to=UploadToPathAndRename("user/", "passport_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    passport_name = models.CharField(max_length=100, blank=True, null=True)
    passport_number = models.CharField(max_length=100, blank=True, null=True)
    visa_number = models.CharField(null=True, blank=True, max_length=255)
    visa_from = models.DateField(blank=True, null=True)
    visa_to = models.DateField(blank=True, null=True)
    visa_file = models.FileField(
        upload_to=UploadToPathAndRename("user/", "visa_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    # Emergency Contact
    emergency_contact_name = models.CharField(null=True, blank=True, max_length=255)
    emergency_contact_relationship = models.CharField(
        null=True, blank=True, max_length=255
    )
    emergency_contact_mobile = models.CharField(null=True, blank=True, max_length=255)
    # Affiliated Institution details
    affiliated_to_institution = models.BooleanField(default=False)
    affiliated_institution = models.CharField(null=True, blank=True, max_length=255)
    affiliated_institution_dor = models.DateField(
        null=True, blank=True, max_length=255, default=None
    )
    affiliated_institution_email = models.EmailField(
        null=True, blank=True, max_length=255
    )
    affiliated_institution_id = models.FileField(
        upload_to=UploadToPathAndRename("contracts/", "affiliated_institution_id_"),
        validators=[
            FileExtensionValidator(["pdf", "jpeg", "jpg"]),
            MaxSizeValidator("5MB"),
        ],
        null=True,
        blank=True,
        default=None,
    )
    personal_email = models.EmailField(blank=True, null=True)
    employee_type = models.CharField(
        max_length=100, blank=True, null=True, choices=EMPLOYEE_TYPE_CHOICES
    )
    supervisor = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.SET_NULL
    )

    def mail_expiry_default():
        return datetime.now() + timedelta(weeks=12)

    mail_expiry = models.DateField(blank=True, null=True, default=mail_expiry_default)

    def __str__(self):
        if self.first_name is None and self.last_name is None:
            return self.email
        else:
            return self.first_name + " " + self.last_name


class LeaveType(models.Model):
    name = models.CharField(max_length=255)
    days = models.IntegerField()

    def __str__(self):
        return self.name


class Leave(models.Model):
    LEAVE_TYPE_CHOICES = (
        ("Casual", "Casual Leave"),
        ("Medical", "Medical Leave"),
        ("Exceptional", "Exceptional Leave"),
        ("Compensation", "Compensation Leave"),
    )
    from_date = models.DateField(blank=True, null=True)
    to_date = models.DateField(blank=True, null=True)
    approved = models.BooleanField(blank=True, null=True)
    applied_by = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        related_name="applied_by",
        blank=True,
        null=True,
    )
    approved_by = models.ForeignKey(
        IFPUser,
        on_delete=models.CASCADE,
        related_name="approved_by",
        blank=True,
        null=True,
    )
    applied_at = models.DateField(blank=True, null=True)
    approved_at = models.DateField(blank=True, null=True)
    leave_type = models.CharField(
        choices=LEAVE_TYPE_CHOICES, max_length=50, default="Casual"
    )
    from_first_half = models.BooleanField(default=True)
    from_second_half = models.BooleanField(default=True)
    to_first_half = models.BooleanField(default=True)
    to_second_half = models.BooleanField(default=True)
    days = models.DecimalField(max_digits=5, decimal_places=1, default=1.0)
    leave_doc = models.FileField(upload_to="Leave_Doc/%Y/%m/%d/", blank=True, null=True)
    leave_cc = models.EmailField(blank=True, null=True)
    reject_reason = models.TextField()

    def __str__(self):
        return str(self.from_date) + " - " + str(self.applied_by)


@receiver(pre_save, sender=IFPUser)
def override_username_with_email(sender, instance, *args, **kwargs):
    instance.username = instance.email


class GuestUser(models.Model):
    user = models.CharField(max_length=255, null=True, blank=True)
    email = models.EmailField(blank=True, null=True)


class MailingList(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(unique=True)
    users = models.ManyToManyField(IFPUser, null=True, blank=True)
    guest_user = models.ManyToManyField("accounts.GuestUser", null=True, blank=True)

    def __str__(self):
        return self.name

    def count(self):
        return self.users.all().count()
