# Generated by Django 4.0.4 on 2022-06-28 19:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_bankaccount_ifpuser_comm_addr_ifpuser_dob_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='bankaccount',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='Active'),
        ),
    ]
