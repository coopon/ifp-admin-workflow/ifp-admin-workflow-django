# Generated by Django 4.2 on 2023-05-03 06:25

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0037_leave_reject_reason_alter_ifpuser_casual_leaves_and_more"),
    ]

    operations = [
        migrations.RenameField(
            model_name="ifpuser",
            old_name="default_leaves",
            new_name="default_casual_leaves",
        ),
        migrations.AddField(
            model_name="ifpuser",
            name="default_medical_leaves",
            field=models.DecimalField(decimal_places=1, default=40.0, max_digits=5),
        ),
    ]
