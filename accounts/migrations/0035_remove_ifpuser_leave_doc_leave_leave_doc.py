# Generated by Django 4.1.7 on 2023-04-25 05:20

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("accounts", "0034_ifpuser_leave_doc"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="ifpuser",
            name="leave_doc",
        ),
        migrations.AddField(
            model_name="leave",
            name="leave_doc",
            field=models.FileField(
                blank=True, null=True, upload_to="Leave_Doc/%Y/%m/%d/"
            ),
        ),
    ]
