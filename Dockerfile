FROM python:3.12-alpine

ENV	PIP_DISABLE_PIP_VERSION_CHECK=1
ENV	PYTHONDONTWRITEBYTECODE=1
ENV	PYTHONUNBUFFERED=1
WORKDIR	/code
COPY	.	.
RUN	pip install -r requirements.txt \
    && python3 manage.py collectstatic
CMD	["sh", "entrypoint.sh"]
