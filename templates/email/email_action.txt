{% extends 'email/email_base.txt' %}

{% block body %}
{{first_name}}
{{ subject }}

{% if reject_reason %}
{{ reject_reason }}
{% endif %}
{{ action_text }}
{{ action_link }}
{% endblock body %}