# Changelog

## [0.4.0] - 2022-08-30

- Fix employee edit page linking from all employees individual selection
- Fix employee edit page -> display picture not uploading
- Finish bank account CRU workflow
- Add contracts app
- Add visa_from, visa_to, passport_name, passport_number, original_institute fields to employee model

## [0.3.0] - 2022-08-23

- All changes until now
- Add employee app
- Add models for employee app

## [0.2.0] - 2022-06-16

### Added

- Add Dockerfile, docker-compose

## [0.1.0] - 2022-05-18

### Added

- Started the a fresh django-project
- Added gitignore, LICENSE, CHANGELOG.md, requirements.txt, dockerignore, VERSION
