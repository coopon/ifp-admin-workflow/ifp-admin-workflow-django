$ git clone https://github.com/wsvincent/djangox.git

# Installation

- Create virtualenv
- Install deps

# Running

- python3 manage.py migrate
- python3 manage.py runserver
- celery -A django_project worker -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler --beat
- docker-compose up -d opera-db
