#!/usr/bin/sh
python3 manage.py migrate

gunicorn django_project.wsgi -w 3 -b 0.0.0.0:8000 --access-logfile '-'
