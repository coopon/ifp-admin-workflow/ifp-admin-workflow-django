from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET,require_POST
from missions.models import Mission, Forex, DailyAllowance
from datetime import date
from missions.forms import NewMissionForm, ResubmitForm, ReimburseForm,ProofResubmitForm, ForexUpdate,DailyAllowanceUpdate
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.db.models import Q
from django.shortcuts import render, redirect, reverse
from accounts.models import IFPUser
from accounts.tasks import send_mail_func
from common.models import Config

@login_required
@require_GET
def missions_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        return render(request,'missions/missions_list.html', {
        "employee":employee,
        "missions_proposed": Mission.objects.filter(status="Proposed"),
        "missions_resubmit": Mission.objects.filter(status="Resubmit"),
        "missions_resubmitted": Mission.objects.filter(status="Resubmitted"),
        "missions_approved": Mission.objects.filter(status="Approved"),
        "missions_completed": Mission.objects.filter(status="Completed"),
        "proof_resubmit": Mission.objects.filter(status="Resubmit Proofs"),
        "proof_validate": Mission.objects.filter(status="Reimburse Review"),
        "missions_reimbursed": Mission.objects.filter(status="Accepted"),
        })
    else:
        return render(request,'missions/missions_list.html', {
        "employee":employee,
        "missions_proposed": Mission.objects.filter(Q(status="Proposed") & Q(proposer=request.user)),
        "missions_resubmit": Mission.objects.filter(Q(status="Resubmit") & Q(proposer=request.user)),
        "missions_resubmitted": Mission.objects.filter(Q(status="Resubmitted") & Q(proposer=request.user)),
        "missions_approved": Mission.objects.filter(Q(status="Approved") & Q(proposer=request.user)),
        "missions_completed": Mission.objects.filter(Q(status="Completed") & Q(proposer=request.user)),
        "proof_resubmit": Mission.objects.filter(Q(status="Resubmit Proofs") & Q(proposer=request.user)),
        "proof_validate": Mission.objects.filter(Q(status="Reimburse Review") & Q(proposer=request.user)),
        "missions_reimbursed": Mission.objects.filter(Q(status="Accepted") & Q(proposer=request.user)),
        })

@login_required
@require_GET
def propose_mission(request):
    employee = IFPUser.objects.get(id=request.user.id)
    form = NewMissionForm()
    return render(request,'missions/propose_mission.html', {'form': form,'employee':employee})

@login_required
@require_POST
def mission_proposed(request):
    employee = IFPUser.objects.get(id=request.user.id)
    form = NewMissionForm(request.POST)
    if form.is_valid():
        mission = form.save(commit=False)
        mission.proposer = request.user
        mission.save()
        config = Config.objects.all().last()
        send_mail_func.apply_async(
                kwargs={
                    "message": "New mission is proposed by " + str(mission.proposer) + " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " is waiting for acknowledgement",
                    "subject": "Opera - Mission Proposed",
                    "to_email": [config.mission.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(config.mission.first_name),

                }
            )
        return redirect('missions_list')
    else:
        return render(request,'missions/propose_mission.html', {'form': form,'employee':employee})

@login_required
@require_GET
def mission_details(request,mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    return render(request, 'missions/mission_details.html', {'mission':mission,'employee':employee})

@login_required
@require_GET
def mission_approved(request,mission_id):
    if request.user.is_superuser:
        mission = Mission.objects.get(id=mission_id)
        mission.status = "Approved"
        mission.save()
        send_mail_func.apply_async(
                kwargs={
                    "message": "Mission is proposed by " +str(mission.proposer) + " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " is Approved",
                    "subject": "Opera - Mission Approved",
                    "to_email": [mission.proposer.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(mission.proposer.first_name),
                }
            )
        return redirect('missions_list')
    else:
        return HttpResponseForbidden()

@login_required
@require_GET
def mission_resubmit(request,mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        mission = Mission.objects.get(id=mission_id)
        mission.status = "Resubmit"
        mission.save()
        form = ResubmitForm(instance=mission)
        return render(request,'missions/mission_resubmit.html', {'mission':mission, 'form':form,'employee':employee})
    else:
        return HttpResponseForbidden()

@login_required
@require_POST
def resubmit_reason(request,mission_id):
    if request.user.is_superuser:
        mission = Mission.objects.get(id=mission_id)
        form = ResubmitForm(request.POST, instance=mission)
        if form.is_valid():
            form.save()
            send_mail_func.apply_async(
                kwargs={
                    "message": "Mission proposed by " + str(mission.proposer)+ " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " is asked to resubmit the proposal.\n "+mission.resubmit_reason,
                    "subject": "Opera - Mission Proposal Resubmit",
                    "to_email": [mission.proposer.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(mission.proposer.first_name),

                }
            )
        return redirect('missions_list')
    else:
        return HttpResponseForbidden()

@login_required
@require_GET
def proof_resubmit(request,mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        mission = Mission.objects.get(id=mission_id)
        mission.status = "Resubmit Proofs"
        mission.save()
        form = ProofResubmitForm(instance=mission)
        return render(request,'missions/resubmit_proof.html', {'mission':mission, 'form':form,'employee':employee})
    else:
        return HttpResponseForbidden()

@login_required
@require_POST
def proof_resubmit_reason(request,mission_id):
    if request.user.is_superuser:
        mission = Mission.objects.get(id=mission_id)
        form = ProofResubmitForm(request.POST, instance=mission)
        if form.is_valid():
            form.save()
            send_mail_func.apply_async(
                kwargs={
                    "message": "Mission proposed by " +str(mission.proposer) + " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " is asked to resubmit the proofs.\n "+mission.resubmit_proof_reason,
                    "subject": "Opera - Mission Proof Resubmit",
                    "to_email": [mission.proposer.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(mission.proposer.first_name),

                }
            )
        return redirect('missions_list')
    else:
        return HttpResponseForbidden()

@login_required
@require_GET
def mission_edit(request, mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    form = NewMissionForm(instance=mission)
    return render(request,'missions/mission_edit.html', {'form':form, 'mission':mission,'employee':employee})

@login_required
@require_POST
def mission_updated(request, mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    form = NewMissionForm(request.POST, instance=mission)
    if form.is_valid():
        mission = form.save(commit=False)
        mission.status = 'Resubmitted'
        mission.resubmit_reason = None
        mission.save()
        config = Config.objects.all().last()
        send_mail_func.apply_async(
                kwargs={
                    "message": "Mission proposed by " +str(mission.proposer) + " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " have resubmitted the mission proposal.",
                    "subject": "Opera - Mission Proposal Resubmitted",
                    "to_email": [config.mission.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(config.mission.first_name),

                }
            )
        return redirect('missions_list')
    else:
        return render(request,'missions/mission_edit.html', {'form':form, 'mission':mission,'employee':employee})

@login_required
@require_GET
def mission_completed(request,mission_id):
    mission = Mission.objects.get(id=mission_id)
    mission.status = 'Completed'
    mission.save()
    return redirect('missions_list')

@login_required
@require_GET
def mission_reimburse(request, mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    form = ReimburseForm()
    return render(request,'missions/mission_reimburse.html', {'mission':mission,'employee':employee,'form':form})

@login_required
@require_POST
def Reimburse_updated(request, mission_id):
    mission = Mission.objects.get(id=mission_id)
    form = ReimburseForm(request.POST, request.FILES)
    if form.is_valid():
        proof = form.save()
        mission.proofs.add(proof)
        mission.save()
        return redirect('mission_proof',mission_id=mission_id)
    else:
        return redirect('mission_reimburse',mission_id=mission_id)

@login_required
def mission_proof(request, mission_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    proofs = mission.proofs.all()
    return render(request,'missions/mission_proof.html', {'mission':mission,'proofs':proofs,'employee':employee})

@login_required
@require_GET
def delete_proof(request, mission_id, proof_id):
    mission = Mission.objects.get(id=mission_id)
    proof = mission.proofs.get(id=proof_id)
    proof.delete()
    return redirect('mission_proof',mission_id=mission_id)

@login_required
@require_GET
def edit_proof(request, mission_id, proof_id):
    employee = IFPUser.objects.get(id=request.user.id)
    mission = Mission.objects.get(id=mission_id)
    proof = mission.proofs.get(id=proof_id)
    form = ReimburseForm(instance=proof)
    return render(request,'missions/edit_proof.html', {'mission':mission,'proof':proof,'form':form,'employee':employee})

@login_required
@require_POST
def edit_proof_updated(request, mission_id, proof_id):
    mission = Mission.objects.get(id=mission_id)
    proof = mission.proofs.get(id=proof_id)
    form = ReimburseForm(request.POST, request.FILES, instance=proof)
    if form.is_valid():
        proof = form.save()
        mission.proofs.add(proof)
        mission.save()
        return redirect('mission_proof',mission_id=mission_id)
    else:
        return redirect('edit_proof',mission_id=mission_id,proof_id=proof_id)

@login_required
@require_GET
def proof_submit(request, mission_id):
    mission = Mission.objects.get(id=mission_id)
    mission.status = 'Reimburse Review'
    mission.resubmit_proof_reason = None
    mission.total_expenditure = 0
    for proof in mission.proofs.all():
        mission.total_expenditure = mission.total_expenditure + proof.amount
    mission.reimburse_amount = mission.total_expenditure - mission.upfront_amount
    mission.save()
    config = Config.objects.all().last()
    send_mail_func.apply_async(
                kwargs={
                    "message": "Mission proposed by " +str(mission.proposer) + " for " + 
                        mission.project.name + " from " 
                        + str(mission.from_date)
                        +" till "+str(mission.to_date)
                        + " with total budget of " + str(mission.total_budget) 
                        + " have submitted the proof of expenditure.",
                    "subject": "Opera - Proof Submitted",
                    "to_email": [config.mission.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(config.mission.first_name),
                }
            )
    send_mail_func.apply_async(
                kwargs={
                    "message": "You have successfully submitted the proof of expenditure. Waiting for administration to validate.",
                    "subject": "Opera - Proof Submitted",
                    "to_email": [mission.proposer.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(mission.proposer.first_name),

                }
            )
    return redirect('mission_details',mission_id=mission_id)

@login_required
@require_GET
def mission_reimbursed(request, mission_id):
    mission = Mission.objects.get(id=mission_id)
    mission.status = 'Accepted'
    mission.save()
    send_mail_func.apply_async(
                kwargs={
                    "message": "The proof of expenditure have successfully validated.",
                    "subject": "Opera - Proof Validated",
                    "to_email": [mission.proposer.email],
                    "action_link":"https://opera.ifpindia.org/missions/list",
                    "first_name": str(mission.proposer.first_name),

                }
            )
    return redirect('missions_list')

@login_required
@require_GET
def settings(request):
    employee = IFPUser.objects.get(id=request.user.id)
    forex = Forex.objects.all()
    daily_allowance = DailyAllowance.objects.all()
    return render(request,'missions/settings.html',{'forex':forex,'daily_allowance':daily_allowance,'employee':employee})

@login_required
@require_GET
def change_forex(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        form = ForexUpdate()
        return render(request,'missions/change_forex.html',{'form':form,'employee':employee})

@login_required
@require_POST
def forex_updated(request):
    form = ForexUpdate(request.POST)
    if request.user.is_superuser and form.is_valid():
        forex = form.save(commit=False)
        forex.date = date.today()
        forex.save()
    return redirect('settings')


@login_required
@require_GET
def change_daily_allowance(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        form = DailyAllowanceUpdate()
        return render(request,'missions/change_daily_allowance.html',{'form':form,'employee':employee})

@login_required
@require_POST
def daily_allowance_updated(request):
    form = DailyAllowanceUpdate(request.POST)
    if request.user.is_superuser and form.is_valid():
        dl = form.save(commit=False)
        dl.date = date.today()
        dl.save()
    return redirect('settings')
