from django import forms
from django.forms import SelectDateWidget
from missions.models import Mission, Proof, Forex, DailyAllowance
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class NewMissionForm(forms.ModelForm):
    class Meta:
        model = Mission
        fields = [
            'project',
            'purpose',
            'remarks',
            'from_date',
            'to_date',
            'location_name',
            'location_type',
            'mission_type',
            'claim_type',
            'leave_required',
            
        ]
        labels = {
            'projects': 'Projects',
            'from_date': 'From',
            'to_date': 'To',
            'location_name': 'Missions Itineray',
            'location_type': 'Location Type',
            'purpose' : 'Purpose of Mission',
            'remarks': 'Remarks',
            'mission_type':'Mission Type',
            'claim_type':'Claim Type',
            'leave_required':'Leave Required',
        }
        widgets = {
            'from_date': forms.DateInput(attrs={"class": "input", "type": "date"}),
            'to_date': forms.DateInput(attrs={"class": "input", "type": "date"}),
        }

class ResubmitForm(forms.ModelForm):
    class Meta:
        model = Mission
        fields = [
            'resubmit_reason',
        ]
        labels = {
            'resubmit_reason': 'Comments',
        }
class ProofResubmitForm(forms.ModelForm):
    class Meta:
        model = Mission
        fields = [
            'resubmit_proof_reason',
        ]
        labels = {
            'resubmit_proof_reason': 'Comments',
        }

class ReimburseForm(forms.ModelForm):
    class Meta:
        model = Proof
        fields = [
            'date',
            'description',
            'amount',
            'currency',
            'document',
        ]
        labels = {
            'date': 'Date',
            'document': 'Upload Proof of Payment',
            'amount': 'Total Amount',
            'description':'Description',
            'currency': 'Currency',
        }
        widgets = {
            
        'date': forms.DateInput(attrs={"class": "input", "type": "date"}),
        }

class ForexUpdate(forms.ModelForm):
    class Meta:
        model = Forex
        fields = [
            'value',
        ]
        labels = {
            'value': 'Enter the INR value for 1 EURO'
        }

class DailyAllowanceUpdate(forms.ModelForm):
    class Meta:
        model = DailyAllowance
        fields = [
            'urban_day_rate',
            'urban_night_rate',
            'rural_day_rate',
            'rural_night_rate',
            'metro_day_rate',
            'metro_night_rate',

        ]
        labels = {
            'urban_day_rate':'Urban Day Rate',
            'urban_night_rate':'Urban Night Rate',
            'rural_day_rate':'Rural Day Rate',
            'rural_night_rate':'Rural Night Rate',
            'metro_day_rate':'Metropolitan Day Rate',
            'metro_night_rate':'Metropolitan Night Rate',
        }