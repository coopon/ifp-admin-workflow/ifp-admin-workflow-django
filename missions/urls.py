from django.urls import path

from missions.views import *

urlpatterns = [
    path("propose", propose_mission, name='propose_mission'),
    path("mission_proposed", mission_proposed, name='mission_proposed'),
    path("list", missions_list, name='missions_list'),
    path("settings/forex", change_forex, name='change_forex'),
    path("settings", settings, name='settings'),
    path("settings/forex_changed", forex_updated, name='forex_updated'),
    path("settings/daily_allowance", change_daily_allowance, name='change_daily_allowance'),
    path("settings/daily_allowance_changed", daily_allowance_updated, name='daily_allowance_updated'),
    path("<str:mission_id>/view", mission_details, name='mission_details'),
    path("<str:mission_id>/approved", mission_approved, name='mission_approved'),
    path("<str:mission_id>/reason", mission_resubmit, name='mission_resubmit'),
    path("<str:mission_id>/resubmit", resubmit_reason, name='resubmit_reason'),
    path("<str:mission_id>/proof_resubmit", proof_resubmit, name='proof_resubmit'),
    path("<str:mission_id>/proof_resubmit_reason", proof_resubmit_reason, name='proof_resubmit_reason'),
    path("<str:mission_id>/edit", mission_edit, name='mission_edit'),
    path("<str:mission_id>/update", mission_updated, name='mission_updated'),
    path("<str:mission_id>/completed", mission_completed, name='mission_completed'),
    path("<str:mission_id>/reimburse", mission_reimburse, name='mission_reimburse'),
    path("<str:mission_id>/reimbursed", Reimburse_updated, name='Reimburse_updated'),
    path("<str:mission_id>/proofs", mission_proof, name='mission_proof'),
    path("<str:mission_id>/proof_submit", proof_submit, name='proof_submit'),
    path("<str:mission_id>/mission_reimbursed", mission_reimbursed, name='mission_reimbursed'),
    path("<str:mission_id>/<str:proof_id>/edit", edit_proof, name='edit_proof'),
    path("<str:mission_id>/<str:proof_id>/edit_update", edit_proof_updated, name='edit_proof_updated'),
    path("<str:mission_id>/<str:proof_id>/delete", delete_proof, name='delete_proof'),





]