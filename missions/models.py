from django.db import models
from common.models import CURRENCY_CHOICES
from projects.models import Project
from common.utils import UploadToPathAndRename, MaxSizeValidator
from django.core.validators import FileExtensionValidator
from uuid import uuid4
from accounts.models import IFPUser
from django.utils.translation import gettext as _


LOCATION_TYPE_CHOICES = [
    ("Big Town", "Big Town"),
    ("Touristic", "Touristic"),
    ("Rest of the place","Rest of the place"),
]

MISSION_TYPE_CHOICES = [
    ("Free of Cost", "Free of Cost"),
    ("Only Transport charges", "Only Transport charges"),
    ("Per Day charges", "Per Day charges"),
]

CLAIM_TYPE_CHOICES = [
    ('Expat','Expat'),
    ('IFP Tariff', 'IFP Tariff'),
    ('Not Required', 'Not Required')
]


class Forex(models.Model):
    date = models.DateField()
    currency = models.CharField(max_length=10, choices=CURRENCY_CHOICES)
    value = models.DecimalField(default = 0,max_digits=10, decimal_places=2)

    def __str__(self):
        return "{} - {}".format(self.date, self.value)



class DailyAllowance(models.Model):
    urban_day_rate = models.DecimalField(max_digits=10, decimal_places=2)
    urban_night_rate = models.DecimalField(max_digits=10, decimal_places=2)
    rural_day_rate = models.DecimalField(max_digits=10, decimal_places=2)
    rural_night_rate = models.DecimalField(max_digits=10, decimal_places=2)
    metro_day_rate = models.DecimalField(max_digits=10, decimal_places=2)
    metro_night_rate = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateField()
    currency = models.CharField(max_length=10, choices=CURRENCY_CHOICES, default='INR')

    def __str__(self):
        return "{}".format(self.date)
    
class Proof(models.Model):
    date = models.DateField()
    document = models.FileField(
        upload_to=UploadToPathAndRename("missions/", "documents"),
        validators=[
            FileExtensionValidator(["jpg", "jpeg", "png","pdf"]),
            MaxSizeValidator("10MB"),
        ],
        
    )
    description = models.TextField(null=True, blank=True)
    amount = models.DecimalField(default=0,max_digits=10, decimal_places=2)
    currency = models.CharField(max_length=10, choices=CURRENCY_CHOICES)

    def __str__(self):
        return "{} - {} {}".format(self.date, self.amount,self.currency)


class Mission(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    from_date = models.DateField()
    to_date = models.DateField()
    proposer = models.ForeignKey(IFPUser, on_delete=models.CASCADE)
    members = models.ManyToManyField(IFPUser,verbose_name=_("members"),related_name="members", null=True, blank=True)
    currency = models.CharField(max_length=10, choices=CURRENCY_CHOICES)
    location_name = models.CharField(max_length=255)
    location_type = models.CharField(max_length=50, choices=LOCATION_TYPE_CHOICES)
    total_budget = models.IntegerField(default=0)
    upfront_amount = models.IntegerField(default=0)
    purpose = models.TextField()
    remarks = models.TextField()
    leave_required = models.BooleanField(default=False)
    mission_type = models.CharField(max_length=50, choices=MISSION_TYPE_CHOICES)
    claim_type = models.CharField(max_length=50, choices=CLAIM_TYPE_CHOICES)
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    MISSION_STATUS_CHOICES = [
        ("Proposed", "Proposed"),
        ('Resubmit', 'Resubmit'),
        ('Resubmitted', 'Resubmitted'),
        ("Approved", "Approved"),
        ("Reimburse", "Reimburse"),
        ("Resubmit Proofs", "Resubmit Proofs"),
        ("Reimburse Review", "Reimburse Review"),
        ("Completed", "Completed"),
    ]
    status = models.CharField(max_length=50, choices=MISSION_STATUS_CHOICES, default="Proposed")
    proofs = models.ManyToManyField(Proof, related_name="mission_proofs", null=True, blank=True)
    resubmit_reason = models.TextField(null=True)
    resubmit_proof_reason = models.TextField(null=True)
    total_expenditure = models.DecimalField(default=0,max_digits=10, decimal_places=2)
    reimburse_amount = models.DecimalField(default=0,max_digits=10, decimal_places=2)

    def __str__(self):
        return "{} - {}".format(self.project.name, self.proposer)