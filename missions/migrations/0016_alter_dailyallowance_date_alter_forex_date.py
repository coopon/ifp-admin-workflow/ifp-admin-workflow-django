# Generated by Django 4.2 on 2023-06-21 11:10

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("missions", "0015_alter_dailyallowance_date_alter_forex_date_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="dailyallowance",
            name="date",
            field=models.DateField(default=datetime.date(2023, 6, 21)),
        ),
        migrations.AlterField(
            model_name="forex",
            name="date",
            field=models.DateField(default=datetime.date(2023, 6, 21)),
        ),
    ]
