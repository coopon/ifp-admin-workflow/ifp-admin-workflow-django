# Generated by Django 4.2 on 2023-06-12 06:48

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("missions", "0010_proof_description"),
    ]

    operations = [
        migrations.RenameField(
            model_name="dailyallowance",
            old_name="day_rate",
            new_name="metro_day_rate",
        ),
        migrations.RenameField(
            model_name="dailyallowance",
            old_name="night_rate",
            new_name="metro_night_rate",
        ),
        migrations.RemoveField(
            model_name="dailyallowance",
            name="location_type",
        ),
        migrations.AddField(
            model_name="dailyallowance",
            name="rural_day_rate",
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="dailyallowance",
            name="rural_night_rate",
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="dailyallowance",
            name="urban_day_rate",
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="dailyallowance",
            name="urban_night_rate",
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="dailyallowance",
            name="currency",
            field=models.CharField(
                choices=[("INR", "Indian Rupee"), ("EUR", "Euro")],
                default="INR",
                max_length=10,
            ),
        ),
        migrations.AlterField(
            model_name="dailyallowance",
            name="date",
            field=models.DateField(default=datetime.date(2023, 6, 12)),
        ),
        migrations.AlterField(
            model_name="forex",
            name="date",
            field=models.DateField(default=datetime.date(2023, 6, 12)),
        ),
    ]
