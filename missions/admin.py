from django.contrib import admin
from missions.models import Mission,Forex,DailyAllowance,Proof

# Register your models here.
admin.site.register(Mission)
admin.site.register(Forex)
admin.site.register(DailyAllowance)
admin.site.register(Proof)
