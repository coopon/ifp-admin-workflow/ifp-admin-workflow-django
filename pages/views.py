from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from accounts.models import IFPUser, Leave
from django_countries import countries
from employee.forms import (
    OnBoardPersonalFrom,
    OnBoardDependentForm,
    OnBoardEmergencyContactForm,
    OnBoardAffiliatedInstitutionForm,
    OnBoardEducationDetailsForm,
    OnBoardEmployeeDetailsForm,
    OnBoardVisa,
    LeaveTopUp,
    BankAccountForm,
    LeaveApplyForm,
    LeaveRejectForm,
    LeaveDocument,
    NewMailingListForm,
    EmployeeFilterForm,
    DepartmentFilterForm,
    ProjectFilterForm,
)
from datetime import datetime, timedelta


class HomePageView(TemplateView):
    template_name = "pages/home.html"


class AboutPageView(TemplateView):
    template_name = "pages/about.html"


class ProfilePageView(TemplateView):
    template_name = "pages/profile.html"


@login_required
def homePage(request):
    employee = IFPUser.objects.get(id=request.user.id)
    today = datetime.today()
    tomorrow = today + timedelta(days=1)
    on_leave_today = Leave.objects.filter(approved=True).filter(
        from_date__range=(today, today)
    )

    on_leave_tomorrow = Leave.objects.filter(approved=True).filter(
        from_date__range=(tomorrow, tomorrow)
    )
    leaves = Leave.objects.filter(approved=None)
    email_expiring_users = IFPUser.objects.filter(
        email__contains="@ifpindia.org",
        mail_expiry__range=[
            datetime.today().strftime("%Y-%m-%d"),
            (datetime.today() + timedelta(28)).strftime("%Y-%m-%d"),
        ],
    ).all()
    email_expired_users = IFPUser.objects.filter(
        email__contains="@ifpindia.org",
        mail_expiry__lt=datetime.today().strftime("%Y-%m-%d"),
    ).all()
    return render(
        request,
        "pages/home.html",
        {
            "employee":employee,
            "leaves": leaves,
            "leaves_today": on_leave_today,
            "leaves_tomorrow": on_leave_tomorrow,
            "email_expiring": email_expiring_users,
            "email_expired": email_expired_users,
        },
    )


@login_required
def editProfile(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET":
        forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(instance=employee),
        "OnBoardDependentForm": OnBoardDependentForm(instance=employee),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(instance=employee),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(instance=employee),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(instance=employee),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(instance=employee),
        "OnBoardVisa": OnBoardVisa(instance=employee),
        }
        return render(
            request, "pages/edit_profile.html", {"emp": employee, "forms": forms,"employee":employee}
        )
    else:
        forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(request.POST, request.FILES, instance=employee),
        "OnBoardDependentForm": OnBoardDependentForm(request.POST, request.FILES, instance=employee),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(request.POST, request.FILES, instance=employee),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(request.POST, request.FILES, instance=employee),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(request.POST, request.FILES, instance=employee),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(request.POST, request.FILES, instance=employee),
        "OnBoardVisa": OnBoardVisa(request.POST, request.FILES, instance=employee),
        }
        if forms["OnBoardPersonalFrom"].is_valid():
            forms["OnBoardPersonalFrom"].save()
        if forms["OnBoardDependentForm"].is_valid():
            forms["OnBoardDependentForm"].save()
        if forms["OnBoardEmergencyContactForm"].is_valid():
            forms["OnBoardEmergencyContactForm"].save()
        if forms["OnBoardAffiliatedInstitutionForm"].is_valid():
            forms["OnBoardAffiliatedInstitutionForm"].save()
        if forms["OnBoardEducationDetailsForm"].is_valid():
            forms["OnBoardEducationDetailsForm"].save()
        if forms["OnBoardEmployeeDetailsForm"].is_valid():
            forms["OnBoardEmployeeDetailsForm"].save()
        if forms["OnBoardVisa"].is_valid():
            forms["OnBoardVisa"].save()
        return redirect("/profile/")


@login_required
def profile(request):
    employee = IFPUser.objects.get(id=request.user.id)
    country = dict(countries)[employee.nationality]
    return render(
        request, "pages/profile.html", {"employee": employee, "country": country}
    )
