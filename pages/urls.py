from django.urls import path
from employee.views import edit_employee
from .views import (
    HomePageView,
    AboutPageView,
    ProfilePageView,
    homePage,
    profile,
    editProfile,
)

urlpatterns = [
    path("", homePage, name="home"),
    path("about/", AboutPageView.as_view(), name="about"),
    path("profile/", profile, name="profile"),
    path("profile/edit/", editProfile, name="edit_profile"),
]
