from datetime import date
from django.db.models import Q


def filter_condition_wrapper(post):
    filters = {}
    filters["visa_from"] = post["visa_from"]
    filters["visa_to"] = post["visa_to"]
    filters["employee_type"] = post["employee_type"]
    filters["email"] = post["email"]
    filters["designation"] = post["designation"]
    filters["department"] = post["department"]
    filters["projects"] = post["projects"]
    a = Q(email__icontains=filters["email"])
    print(a)
    return (filters, a)
