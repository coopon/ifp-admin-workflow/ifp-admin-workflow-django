from django import forms
from django.forms import SelectDateWidget

from accounts.models import IFPUser, BankAccount, Leave, MailingList, GuestUser
from department.models import Department
from projects.models import Project
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from common.forms import MultipleFileInput


class OnBoardPersonalFrom(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "title",
            "display_picture",
            "first_name",
            "last_name",
            "dob",
            "gender",
            "phone",
            "personal_email",
            "comm_addr",
            "perm_addr",
            "permanent_address_proof",
        )
        labels = {
            "title": "Title",
            "display_picture": "Recent Photo",
            "gender": "Gender",
            "dob": "Date of Birth",
            "phone": "Phone Number",
            "personal_email": "Personal Email",
            "comm_addr": "Current Address",
            "perm_addr": "Permanent Address",
            "permanent_address_proof": "Permanent Address Proof",
        }
        widgets = {
            "dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "personal_email": forms.EmailInput(attrs={"class": "form-control input"}),
        }


class OnBoardDependentForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "spouse_name",
            "spouse_dob",
            "spouse_relation",
            "spouse_id_proof",
            "child1_name",
            "child1_dob",
            "child1_relation",
            "child1_id_proof",
            "child2_name",
            "child2_dob",
            "child2_relation",
            "child2_id_proof",
        )
        labels = {
            "spouse_name": "Spouse Name",
            "spouse_dob": "Date of Birth",
            "spouse_relation": "Relationship",
            "spouse_id_proof": "ID Proof",
            "child1_name": "Child1 Name",
            "child1_dob": "Date of Birth",
            "child1_relation": "Relationship",
            "child1_id_proof": "ID Proof",
            "child2_name": "Child2 Name",
            "child2_dob": "Date of Birth",
            "child2_relation": "Relationship",
            "child2_id_proof": "ID Proof",
        }
        widgets = {
            "spouse_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "child1_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "child2_dob": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }


class OnBoardEmergencyContactForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "emergency_contact_name",
            "emergency_contact_relationship",
            "emergency_contact_mobile",
        )
        labels = {
            "emergency_contact_name": "Name",
            "emergency_contact_relationship": "Relationship",
            "emergency_contact_mobile": "Mobile",
        }


class OnBoardAffiliatedInstitutionForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "affiliated_institution",
            "affiliated_institution_dor",
            "affiliated_institution_email",
            "affiliated_institution_id",
        )
        labels = {
            "affiliated_institution": "Name of the Affiliated Institution",
            "affiliated_institution_dor": "Date of Registration in the Institution",
            "affiliated_institution_email": "Email provided by your Institution",
            "affiliated_institution_id": "Proof of Affiliation (ID / Letter of Affiliation)",
        }
        widgets = {
            "affiliated_institution_dor": forms.DateInput(
                attrs={"class": "input", "type": "date"}
            ),
        }


class OnBoardEducationDetailsForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "institute_name",
            "degree_name",
            "year_of_passing",
            "mark_percent",
            "degree_certificate",
        )
        labels = {
            "institute_name": "Institute/University Name",
            "degree_name": "Degree/Course Name",
            "year_of_passing": "Year of Passing",
            "mark_percent": "Total Mark Percentage",
            "degree_certificate": "Upload Degree Certificate",
        }
        widgets = {
            "year_of_passing": forms.DateInput(
                attrs={"class": "input", "type": "date"}
            ),
        }


class OnBoardEmployeeDetailsForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "email",
            "employee_type",
            "emp_id",
            "doj",
            "mail_expiry",
            "designation",
            "supervisor",
            "pi",
        )
        labels = {
            "email": "Email",
            "employee_type": "Employee Type",
            "emp_id": "Employee ID",
            "doj": "Date of Joining",
            "designation": "Designation",
            "pi": "Principal Investigator",
            "supervisor": "Supervisor Name",
            "mail_expiry": "Mail Expiry",
        }
        widgets = {
            "doj": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "email": forms.EmailInput(attrs={"class": "form-control input"}),
            "mail_expiry": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }


class OnBoardVisa(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "passport_name",
            "passport_number",
            "passport",
            "visa_number",
            "visa_from",
            "visa_to",
            "visa_file",
        )
        labels = {
            "passport": "Passport Copy",
            "passport_name": "Name as in Passport",
            "passport_number": "Passport Number",
            "visa_number": "VISA Number",
            "visa_from": "VISA From",
            "visa_to": "VISA To",
            "visa_file": "VISA Copy",
        }
        widgets = {
            "visa_from": forms.DateInput(attrs={"class": "input", "type": "date"}),
            "visa_to": forms.DateInput(attrs={"class": "input", "type": "date"}),
        }


class LeaveTopUp(forms.ModelForm):
    class Meta:
        model = IFPUser
        fields = (
            "default_casual_leaves",
            "default_medical_leaves",
            "casual_leaves",
            "medical_leaves",
        )
        labels = {
            "default_casual_leaves": "Yearly casual leaves",
            "default_medical_leaves": "Yearly medical leaves",
            "casual_leaves": "Remaining casual leaves",
            "medical_leaves": "Remaining medical leaves",
        }


class DepartmentFilterForm(forms.ModelForm):
    department = forms.ModelChoiceField(
        queryset=Department.objects.all(), required=False, initial=0
    )

    class Meta:
        model = Department
        fields = ("department",)
        labels = {
            "department": "Department",
        }
        widgets = {
            "department": forms.ModelChoiceField(queryset=Department.objects.all()),
        }


class EmployeeFilterForm(forms.ModelForm):
    class Meta:
        model = IFPUser
        DESIGNATION_CHOICES = [
            "Technical Agent",
            "Technical Assistant",
            "Technician",
            "Engineer Assistant",
            "Research Fellow - JR",
            "Research Fellow - SR",
            "Research Director",
        ]
        EMPLOYEE_TYPE_CHOICES = ["Student", "Expat", "Indian"]
        fields = (
            "email",
            "designation",
            "visa_from",
            "visa_to",
            "employee_type",
        )

        labels = {
            "email": "Email",
            "designation": "Designation",
            "visa_from": "Visa From",
            "visa_to": "Visa To",
            "employee_type": "Employee Type",
        }

        widgets = {
            "designation": forms.Select(
                choices=[(x, x) for x in DESIGNATION_CHOICES],
            ),
            "employee_type": forms.Select(
                choices=[(x, x) for x in EMPLOYEE_TYPE_CHOICES],
            ),
            "email": forms.TextInput(
                attrs={"class": "form-control input", "type": "text"}
            ),
            "visa_from": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "visa_to": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
        }


class BankAccountForm(forms.ModelForm):
    class Meta:
        model = BankAccount
        fields = (
            "acc_name",
            "acc_no",
            "acc_type",
            "bank_name",
            "branch",
            "uid_type",
            "uid",
            "pan",
            "bank_details_proof",
        )

        labels = {
            "acc_name": "Account holder's name",
            "acc_no": "Account Number",
            "bank_name": "Bank Name",
            "branch": "Branch Name",
            "uid_type": "Branch Identifier Type",
            "uid": "Branch Identifier Code",
            "acc_type": "Account Type",
            "pan": "PAN Number",
            "bank_details_proof": "Bank Passbook Copy",
        }


class LeaveApplyForm(forms.ModelForm):
    class Meta:
        model = Leave
        fields = (
            "from_date",
            "from_first_half",
            "from_second_half",
            "to_date",
            "to_first_half",
            "to_second_half",
            "leave_type",
            "leave_cc",
        )
        labels = {
            "from_date": "Leave from",
            "to_date": "Leave until",
            "from_first_half": "First Half",
            "from_second_half": "Second Half",
            "to_first_half": "First Half",
            "to_second_half": "Second Half",
            "leave_cc": "Add CC Email (If Needed)",
        }
        widgets = {
            "from_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "to_date": forms.DateInput(
                attrs={"class": "form-control input", "type": "date"}
            ),
            "leave_cc": forms.TextInput(
                attrs={"class": "form-control input", "type": "text"}
            ),
        }


class LeaveRejectForm(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ("reject_reason",)
        labels = {"reject_reason": "Comments"}
        widget = {
            "reject_reason": forms.Textarea(attrs={"class": "textarea"}),
        }


class LeaveDocument(forms.ModelForm):
    class Meta:
        model = Leave
        fields = ("leave_doc",)
        labels = {"leave_doc": ""}
        widget = {
            "leave_doc": MultipleFileInput(attrs={"multiple": True}),
        }


class NewMailingListForm(forms.ModelForm):
    class Meta:
        model = MailingList
        fields = ("name", "email", "users")
        labels = {"name": "Mailing List Name", "email": "Mailing List Email Address"}


class NewGuestMailingForm(forms.ModelForm):
    class Meta:
        model = GuestUser
        fields = ("user", "email")
        labels = {"user": "Guest Name", "guest_email": "Guest Email Address"}
        widgets = {
            "email": forms.TextInput(
                attrs={"class": "form-control input", "type": "text"}
            ),
        }


class ProjectFilterForm(forms.ModelForm):
    projects = forms.ModelChoiceField(
        queryset=Project.objects.all(), required=False, initial=0
    )

    class Meta:
        model = Project
        fields = ("projects",)
        labels = {"projects": "Project Name"}
        widgets = {
            "projects": forms.ModelChoiceField(queryset=Project.objects.all()),
        }
