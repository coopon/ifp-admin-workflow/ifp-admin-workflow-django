import json


class EmployeeFilter:
    name = None
    email = None
    designation = None
    visa_from = None
    visa_to = None
    employee_type = None

    def __init__(self, filter_obj):
        if 'name' in filter_obj:
            self.name = filter_obj['name']
        if 'email' in filter_obj:
            self.email = filter_obj['email']
        if 'designation' in filter_obj:
            self.designation = filter_obj['designation']
        if 'visa_from' in filter_obj:
            self.visa_from = filter_obj['visa_from']
        if 'visa_to' in filter_obj:
            self.visa_to = filter_obj['visa_to']
        if 'employee_type' in filter_obj:
            self.employee_type = filter_obj['employee_type']

    def get_dept(self):
        if self.name == None:
             return ''
        else:
            return self.name

    def get_email(self):
        if self.email == None:
             return ''
        else:
            return self.email

    def get_designation(self):
        if self.designation == None:
            return ''
        else:
            return self.designation


    def get_visa_from(self):
        if self.visa_from == None:
             return ''
        else:
            return self.visa_from

    def get_visa_to(self):
        if self.visa_to == None:
            return ''
        else:
            return self.visa_to
    def get_employee_type(self):
        if self.employee_type == None:
            return ''
        else:
            return self.employee_type
