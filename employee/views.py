import csv
from datetime import datetime, timedelta, date
from decimal import Decimal
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods,require_GET,require_POST
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django_countries import countries
from django.db.models import Q
from accounts.models import IFPUser, BankAccount, Leave, MailingList,GuestUser
from accounts.tasks import send_mail_func
from common.models import Config
from missions.models import Mission
from contracts.models import Contract, ContractHistory
from employee.forms import (
    OnBoardPersonalFrom,
    OnBoardDependentForm,
    OnBoardEmergencyContactForm,
    OnBoardAffiliatedInstitutionForm,
    OnBoardEducationDetailsForm,
    OnBoardEmployeeDetailsForm,
    OnBoardVisa,
    LeaveTopUp,
    BankAccountForm,
    LeaveApplyForm,
    LeaveRejectForm,
    LeaveDocument,
    NewMailingListForm,
    NewGuestMailingForm,
    EmployeeFilterForm,
    DepartmentFilterForm,
    ProjectFilterForm,
)
from employee.utils import filter_condition_wrapper
from common.models import CalendarLeaves


@login_required
@require_GET
def employee_list(request):
    employee_obj = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET" and request.user.is_superuser:
        emp_form = EmployeeFilterForm()
        dept_form = DepartmentFilterForm()
        project_form = ProjectFilterForm()
        employees = IFPUser.objects.filter(is_active=True).all()
        return render(
            request,
            "employee/list.html",
            {
                "employees": employees,
                "employee":employee_obj,
                "emp_form": emp_form,
                "dept_form": dept_form,
                "project_form": project_form,
            },
        )
    else:
        emp_form = EmployeeFilterForm()
        dept_form = DepartmentFilterForm()
        project_form = ProjectFilterForm()
        employees = employee_obj.project_members.all()[0].members.all()
        return render(
            request,
            "employee/list.html",
            {
                "employee":employee_obj,
                "employees": employees,
                "emp_form": emp_form,
                "dept_form": dept_form,
                "project_form": project_form,
            },
        )


@login_required
@require_http_methods(["POST"])
def employee_delete(request, empid=None):
    if empid:
        emp = get_object_or_404(IFPUser, emp_id=empid)
        emp.is_active = False
        emp.save()
    return redirect(reverse("employee_list"))


@login_required
@require_GET
def employee_list_filter(request):
    filters = request.POST
    filters, q = filter_condition_wrapper(filters)
    emp_form = EmployeeFilterForm(
        data={
            "email": filters["email"],
            "designation": filters["designation"],
            "visa_from": filters["visa_from"],
            "visa_to": filters["visa_to"],
            "employee_type": filters["employee_type"],
        }
    )
    dept_form = DepartmentFilterForm(data={"department": filters["department"]})
    project_form = ProjectFilterForm(data={"projects": filters["projects"]})
    filters_list = {}
    for key, value in filters.items():
        if value != "":
            filters_list[key] = value
    print(filters_list)
    employees = IFPUser.objects.filter(q)
    print(employees)
    return render(
        request,
        "employee/list.html",
        {
            "employees": employees,
            "emp_form": emp_form,
            "dept_form": dept_form,
            "project_form": project_form,
            "filters": filters,
        },
    )


#    return HttpResponse("Okay")


@login_required
@require_GET
def employee_list_export(filter_obj):
    employees = IFPUser.objects.all()
    today = str(datetime.today().date())
    response = HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="'
            + "employee"
            + "-list-CSV-export"
            + today
            + '.csv"'
        },
    )
    writer = csv.writer(response)
    writer.writerow(
        [
            "Emp ID",
            "Name",
            "Gender",
            "Date of Birth",
            "Official Email",
            "Personal Email",
            "Date joined",
            "Phone no",
            "Permenent Address",
            "Contract From",
            "Contract To",
            "Contract Type",
            "Nationality",
            "Designation",
            "Passport name",
            "Passport number",
            "Visa number",
            "Visa from",
            "Visa to",
            "Employee type",
            "Department name",
            "Leaves available",
        ]
    )
    for emp in employees:
        #        print(emp)
        writer.writerow(
            [
                emp.emp_id,
                emp.first_name + emp.last_name,
                emp.gender,
                emp.dob,
                emp.email,
                emp.personal_email,
                emp.doj,
                emp.phone,
                emp.perm_addr,
                emp.contract_from,
                emp.contract_to,
                emp.contract_type,
                emp.nationality,
                emp.designation,
                emp.passport_name,
                emp.passport_number,
                emp.visa_number,
                emp.visa_from,
                emp.visa_to,
                emp.employee_type,
                emp.casual_leaves,
            ]
        )
    return response


@login_required
@require_http_methods(["GET", "POST"])
def employee_detail(request, empid=None):
    employee = IFPUser.objects.get(emp_id=empid)
    contract_history = ContractHistory.objects.filter(employee=employee)
    missions = Mission.objects.filter(proposer=employee)
    if empid:
        emp = get_object_or_404(IFPUser, emp_id=empid)
        if request.method == "POST" and request.user.is_superuser:
            bankform = BankAccountForm(request.POST)
            if bankform.is_valid():
                a = bankform.save()
                bankformobject = get_object_or_404(BankAccount, id=a.pk)
                emp.bank_accounts.add(bankformobject)
        else:
            bankform = BankAccountForm()
        country = dict(countries)[employee.nationality]
        leaves = Leave.objects.filter(applied_by=employee).order_by("-from_date")
        return render(
            request,
            "pages/profile.html",
            {"employee": employee, "country": country, "leaves": leaves, "contract_history":contract_history,"missions":missions},
        )
        # return render(request, "pages/profile.html", {"emp": emp, "bankform": bankform})


@login_required
@require_http_methods(["GET", "POST"])
def on_board(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET" and request.user.is_superuser:
        forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(),
        "OnBoardDependentForm": OnBoardDependentForm(),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(),
        "OnBoardVisa": OnBoardVisa(),
        }
        return render(request, "employee/on_board.html", {
            "forms":forms, "employee":employee,
            }
        )
    else:
        form = OnBoardEmployeeDetailsForm(request.POST)
        if form.is_valid():
            empid = form.cleaned_data['emp_id']
            form.save()
            new_employee = IFPUser.objects.get(emp_id=empid)
        form = OnBoardPersonalFrom(request.POST, request.FILES, instance=new_employee)
        if form.is_valid():
            form.save()
        form = OnBoardDependentForm(request.POST, request.FILES, instance=new_employee)
        if form.is_valid():
            form.save()
        form = OnBoardEmergencyContactForm(request.POST, instance=new_employee)
        if form.is_valid():
            form.save()
        form = OnBoardAffiliatedInstitutionForm(request.POST, request.FILES, instance=new_employee)
        if form.is_valid():
            form.save()
        form = OnBoardEducationDetailsForm(request.POST, request.FILES, instance=new_employee)
        if form.is_valid():
            form.save()
        form = OnBoardVisa(request.POST, request.FILES, instance=new_employee)
        if form.is_valid():
            form.save()
        if new_employee.doj:
            new_employee.default_casual_leaves = (12 - new_employee.doj.month) * 2.5
            new_employee.casual_leaves = new_employee.default_casual_leaves
            new_employee.new = False
            new_employee.save()
        return redirect(reverse("employee_list"))


@login_required
@require_http_methods(["POST"])
def disable_bank_account(request, empid=None, bank_id=None):
    if bank_id and empid:
        bank = get_object_or_404(BankAccount, id=bank_id)
        bank.is_active = False
        bank.save()
    return redirect(reverse("employee_detail", kwargs={"empid": empid}))


@login_required
@require_http_methods(["GET", "POST"])
def edit_employee(request, empid):
    employee = IFPUser.objects.get(emp_id=empid)
    if request.method == "GET" and request.user.is_superuser:
        forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(instance=employee),
        "OnBoardDependentForm": OnBoardDependentForm(instance=employee),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(instance=employee),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(instance=employee),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(instance=employee),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(instance=employee),
        "OnBoardVisa": OnBoardVisa(instance=employee),
        }
        leave_topup = LeaveTopUp(instance=employee)
        return render(
            request, "employee/edit_details.html", {"emp": employee, "forms": forms, "topup_form":leave_topup,"employee":employee}
        )
    else:
        forms = {
        "OnBoardPersonalFrom": OnBoardPersonalFrom(request.POST, request.FILES, instance=employee),
        "OnBoardDependentForm": OnBoardDependentForm(request.POST, request.FILES, instance=employee),
        "OnBoardEmergencyContactForm": OnBoardEmergencyContactForm(request.POST, request.FILES, instance=employee),
        "OnBoardAffiliatedInstitutionForm": OnBoardAffiliatedInstitutionForm(request.POST, request.FILES, instance=employee),
        "OnBoardEducationDetailsForm": OnBoardEducationDetailsForm(request.POST, request.FILES, instance=employee),
        "OnBoardEmployeeDetailsForm": OnBoardEmployeeDetailsForm(request.POST, request.FILES, instance=employee),
        "OnBoardVisa": OnBoardVisa(request.POST, request.FILES, instance=employee),
        }
        if forms["OnBoardPersonalFrom"].is_valid():
            employee = forms["OnBoardPersonalFrom"].save()
        if forms["OnBoardDependentForm"].is_valid():
            employee = forms["OnBoardDependentForm"].save()
        if forms["OnBoardEmergencyContactForm"].is_valid():
            employee = forms["OnBoardEmergencyContactForm"].save()
        if forms["OnBoardAffiliatedInstitutionForm"].is_valid():
            employee = forms["OnBoardAffiliatedInstitutionForm"].save()
        if forms["OnBoardEducationDetailsForm"].is_valid():
            employee = forms["OnBoardEducationDetailsForm"].save()
        if forms["OnBoardEmployeeDetailsForm"].is_valid():
            employee = forms["OnBoardEmployeeDetailsForm"].save()
        if forms["OnBoardVisa"].is_valid():
            employee = forms["OnBoardVisa"].save()
        leave_topup = LeaveTopUp(request.POST, instance=employee)
        if leave_topup.is_valid():
            leave_topup.save()
        if employee.new:
            if employee.doj:
                employee.default_casual_leaves = (12 - employee.doj.month) * 2.5
                employee.casual_leaves = employee.default_casual_leaves
                employee.new = False
        employee.save()
        return redirect("/employee/")


@login_required
@require_http_methods(["GET", "POST"])
def new_bank_account(request, empid):
    employee = IFPUser.objects.get(emp_id=empid)
    bank_account = BankAccount()
    if request.method == "GET":
        form = BankAccountForm(instance=bank_account)
        return render(
            request,
            "employee/bank_account_create.html",
            {"emp": employee, "form": form,"employee":employee},
        )
    elif request.method == "POST":
        form = BankAccountForm(request.POST, instance=bank_account)
        if form.is_valid():
            acc = form.save()
            employee.bank_accounts.add(acc)
        return redirect("employee_detail", empid=empid)
    else:
        return redirect("/employee/")


@login_required
@require_http_methods(["GET", "POST"])
def apply_leave(request):
    leave = Leave()
    employee = IFPUser.objects.get(id=request.user.id)
    remaining_leave = int(employee.casual_leaves)
    error_code = 0
    if remaining_leave > 0:
        if request.method == "GET" and (request.user.is_superuser or employee.contract_type == 'CDD' or employee.contract_type == 'CDI'):
            form = LeaveApplyForm(instance=leave)
            return render(
                request, "employee/leave_apply.html", {"emp": employee,"employee":employee, "form": form, "remaining_leave":remaining_leave,  "error_code": error_code}
            )
        elif request.method == "POST" and (request.user.is_superuser or employee.contract_type == 'CDD' or employee.contract_type == 'CDI'):
            form = LeaveApplyForm(request.POST, instance=leave)
            if form.is_valid():
                days = form.cleaned_data["to_date"] - form.cleaned_data["from_date"]
                leave_cc = form.cleaned_data["leave_cc"]
                temp_to_date = form.cleaned_data["to_date"]
                temp_from_date = form.cleaned_data["from_date"]
                #Same day leave validation
                old_leaves = Leave.objects.filter(applied_by=request.user.id)
                if old_leaves:
                    for old_leave in old_leaves:
                        old_to_date = old_leave.to_date
                        if old_to_date > temp_from_date:
                            error_code = 3
                            return render(
                request, "employee/leave_apply.html", {"emp": employee, "employee":employee,"form": form, "remaining_leave":remaining_leave,  "error_code": error_code}
            )
                # skip weekends
                weekend = 0
                pub_holiday = 0
                if temp_from_date > temp_to_date:
                    error_code = 1
                    return render(
                request, "employee/leave_apply.html", {"emp": employee, "employee":employee,"form": form, "remaining_leave":remaining_leave,  "error_code": error_code}
            )
                temp_to_date = str(temp_to_date) + "-00:00:00"
                temp_from_date = str(temp_from_date) + "-00:00:00"
                temp_to_date = datetime.strptime(temp_to_date, "%Y-%m-%d-%H:%M:%S")
                temp_from_date = datetime.strptime(temp_from_date, "%Y-%m-%d-%H:%M:%S")
                temp1_from_date = temp_from_date
                for x in range(days.days):
                    if (
                        temp_from_date.weekday() == 5 or temp_from_date.weekday() == 6
                    ) and temp_from_date != temp_to_date:
                        weekend = weekend + 1
                    temp_from_date = temp_from_date + timedelta(days=1)
                # skip Public Holidays
                cal_from_date = form.cleaned_data["from_date"]
                cal_to_date = form.cleaned_data["to_date"]
                cal_Obj = CalendarLeaves.objects.all()
                for j in range(days.days):
                    for x in range(int(CalendarLeaves.objects.count())):
                        temp = cal_Obj[x].date
                        if (
                            (cal_from_date == temp)
                            and (cal_from_date != cal_to_date)
                            and (
                                temp1_from_date.weekday() != 5
                                or temp1_from_date.weekday() != 6
                            )
                        ):
                            pub_holiday = pub_holiday + 1
                    cal_from_date = cal_from_date + timedelta(days=1)
                    temp1_from_date = temp1_from_date + timedelta(days=1)
                days = days.days + 1
                leavetype = form.cleaned_data["leave_type"]
                from_first_half = form.cleaned_data["from_first_half"]
                from_second_half = form.cleaned_data["from_second_half"]
                to_first_half = form.cleaned_data["to_first_half"]
                to_second_half = form.cleaned_data["to_second_half"]
                if leavetype == "Medical":
                    if days > employee.medical_leaves:
                        error_code = 2
                        return render(
                request, "employee/leave_apply.html", {"emp": employee,"employee":employee, "form": form, "remaining_leave":remaining_leave,  "error_code": error_code}
            )
                    if (
                        (from_first_half == True and from_second_half != True)
                        or (from_first_half != True and from_second_half == True)
                    ) or (
                        (to_first_half == True and to_second_half != True)
                        or (to_first_half != True and to_second_half == True)
                    ):
                        days = days - Decimal(0.5)
                        employee.medical_leaves = employee.medical_leaves - days
                    else:
                        employee.medical_leaves = employee.medical_leaves - days
                else:
                    if days > employee.casual_leaves:
                        error_code = 2
                        return render(
                request, "employee/leave_apply.html", {"emp": employee,"employee":employee, "form": form, "remaining_leave":remaining_leave,  "error_code": error_code}
            )
                    if (
                        (from_first_half == True and from_second_half != True)
                        or (from_first_half != True and from_second_half == True)
                    ) or (
                        (to_first_half == True and to_second_half != True)
                        or (to_first_half != True and to_second_half == True)
                    ):
                        days = days - Decimal(0.5) - weekend - pub_holiday
                        employee.casual_leaves = employee.casual_leaves - days
                    else:
                        days = days - weekend - pub_holiday
                        employee.casual_leaves = employee.casual_leaves - days 
                        print(employee.casual_leaves," days ", days, " pub_holiday ", pub_holiday, " weekend ", weekend)
                employee.save()
                leave.days = days
                leave = form.save()
                leave.applied_by = employee
                leave.applied_at = datetime.now()
                leave.save()
                config = Config.objects.all().last()
                send_mail_func.apply_async(
            kwargs={
                    "message": str(employee.first_name +" "+employee.last_name+" has applied for "
                    + leave.leave_type +" leave for "+str(leave.days) +" days from "+ str(leave.from_date)+" till "+str(leave.to_date)+
                    ". "),
                    "subject": "Opera - Leave Applied",
                    "to_email": [config.mission.email,leave_cc],
                    "first_name": str(config.mission.first_name),

                }
            )
            return render(request, "employee/leave_success.html",{"employee":employee})
    else:
        return render(request,"employee/leave_unavailable.html", {"remaining_leave":remaining_leave,"employee":employee} )


@login_required
@require_http_methods(["GET", "POST"])
def approve_leave(request):
    leave = Leave()
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET":
        form = LeaveApplyForm(instance=leave)
        return render(
            request, "employee/leave_apply.html", {"emp": employee, "form": form,"employee":employee}
        )
    elif request.method == "POST" and request.user.is_superuser:
        leave_id = request.POST["leave-id"]
        leave = Leave.objects.get(id=leave_id)
        leave.approved_by = employee
        leave.approved_at = datetime.now()
        leave.approved = True
        leave.save()
        send_mail_func.apply_async(
            kwargs={
                    "message": "Your "+ leave.leave_type +" leave for "+str(leave.days) +" days from "+ str(leave.from_date)+" till "+str(leave.to_date)+
                    " has been approved.",
                    "subject": "Opera - Leave Approved",
                    "to_email": [leave.applied_by.email],
                    "action_link":"https://opera.ifpindia.org/employee/my_leave",
                    "first_name": str(leave.applied_by.first_name),

                }
            )
        return render(request, "employee/leave_approved.html")

@login_required
@require_http_methods(["GET", "POST"])
def leave_reject(request, id=None):
    leave = Leave()
    current_leave = Leave.objects.get(id=id)
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET" and request.user.is_superuser:
        form = LeaveRejectForm(instance=leave)
        return render(
            request, "employee/leave_reject.html", {"form": form, "leave":current_leave,"employee":employee}
        )
    elif request.method == "POST" and request.user.is_superuser:
        form = LeaveRejectForm(request.POST,instance=leave)
        leave = Leave.objects.get(id=id)
        if form.is_valid():
            leave.reject_reason = form.cleaned_data['reject_reason']
            days = leave.days
            if leave.leave_type == "Casual":
                employee.casual_leaves = employee.casual_leaves + days
            else:
                employee.medical_leaves = employee.medical_leaves + days
            leave.approved_by = employee
            leave.approved_at = datetime.now()
            leave.approved = False
            employee.save()
            leave.save()
            send_mail_func.apply_async(
            kwargs={
                    "message": "Your "+ leave.leave_type +" Leave for "+str(leave.days) +" days from "+ str(leave.from_date)+" till "+str(leave.to_date)+" has been rejected.",
                    "subject": "Opera - Leave Rejected",
                    "to_email": [leave.applied_by.email],
                    "action_link":"https://opera.ifpindia.org/employee/my_leave",
                    "reject_reason": leave.reject_reason,
                    "first_name": str(leave.applied_by.first_name),
                }
            )
            return redirect("/")


@login_required
@require_GET
def bank_account_view(request, bank_id=None, empid=None):
    employee = IFPUser.objects.get(emp_id=empid)
    bank_account = BankAccount.objects.get(id=bank_id)
    return render(
        request,
        "employee/bank_account_view.html",
        {"emp": employee, "acc": bank_account,"employee":employee},
    )


@login_required
@require_http_methods(["GET", "POST"])
def bank_account_edit(request, bank_id=None, empid=None):
    employee = IFPUser.objects.get(emp_id=empid)
    bank_account = BankAccount.objects.get(id=bank_id)
    if request.method == "GET":
        form = BankAccountForm(instance=bank_account)
        return render(
            request,
            "employee/bank_account_edit.html",
            {"emp": employee, "form": form, "acc": bank_account,"employee":employee},
        )
    elif request.method == "POST":
        form = BankAccountForm(request.POST, instance=bank_account)
        if form.is_valid():
            form.save()
        return redirect(
            reverse("bank_account_view", kwargs={"empid": empid, "bank_id": bank_id})
        )


@login_required
@require_GET
def mailing_list_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.user.is_superuser:
        mail_lists = MailingList.objects.all()
        users = IFPUser.objects.all()
    else:
        mail_lists = MailingList.objects.filter(users=employee)
        users = IFPUser.objects.all()
    return render(
            request,
            "employee/mailing_list_list.html",
            {"mail_lists": mail_lists, "users": users,"employee":employee},
        )


@login_required
def mailing_list_expiring_view(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET" and request.user.is_superuser:
        query_condition1 = Q(mail_expiry__lte=(datetime.today() + timedelta(28)))
        query_condition2 = Q(email__iendswith="@ifpindia.org")
        emails = IFPUser.objects.filter(
            query_condition1 & query_condition2
        ).all()
        return render(
            request, "employee/mailing_list_expiring.html", {"emails": emails,"employee":employee}
        )
    else:
        a = request.POST
        user = IFPUser.objects.get(email=a["email"])
        extension = int(a["weeks"]) * 7
        user.mail_expiry = user.mail_expiry + timedelta(extension)
        user.save()
        return redirect(reverse("mailing_list_expiring_view"))


@login_required
@require_GET
def mailing_list_view(request, ml_id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    mail_list = MailingList.objects.get(id=ml_id)
    return render(
        request,
        "employee/mailing_list_view.html",
        {"mail_list": mail_list,"employee":employee},
    )

@login_required
@require_GET
def mailing_list_delete(request, ml_id=None):
    if request.user.is_superuser:
        employee = IFPUser.objects.get(id=request.user.id)
        mail_list = MailingList.objects.get(id=ml_id)
        mail_list.delete()
        return redirect(reverse("mailing_list_list"))

@login_required
@require_http_methods(["GET", "POST"])
def ml_add_guest(request, ml_id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    mail_list = MailingList.objects.get(id=ml_id)
    if request.method == "GET" and request.user.is_superuser:
        form = NewGuestMailingForm()
        return render(
            request,
            "employee/mailing_list_guest_add.html",
            {"form":form,"employee":employee,"mail_list":mail_list},
        )
    else:
        form = NewGuestMailingForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['user']
            new = form.save()
            id = new.id
            guest_user = get_object_or_404(GuestUser, id=id)
            mail_list.guest_user.add(guest_user)
            return redirect(
                reverse("mailing_list_view", kwargs={"ml_id": ml_id})
            )

@login_required
@require_GET
def ml_delete_guest(request, ml_id=None, id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    mail_list = MailingList.objects.get(id=ml_id)
    if request.user.is_superuser:
        guest_user = get_object_or_404(GuestUser, id=id)
        mail_list.guest_user.remove(guest_user)
        guest_user.delete()
        return redirect(
                reverse("mailing_list_view", kwargs={"ml_id": ml_id})
            )

@login_required
@require_http_methods(["GET", "POST"])
def mailing_list_edit(request, ml_id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    mail_list = MailingList.objects.get(id=ml_id)
    if request.method == "GET" and request.user.is_superuser:
        form = NewMailingListForm(instance=mail_list)
        return render(
            request,
            "employee/mailing_list_edit.html",
            {"mail_list": mail_list, "form": form,"employee":employee},
        )
    else:
        form = NewMailingListForm(request.POST, instance=mail_list)
        if form.is_valid():
            form.save()
        return redirect(reverse("mailing_list_view", kwargs={"ml_id": ml_id}))


@login_required
@require_GET
def mailing_list_export(request, ml_id=None):
    mail_list = MailingList.objects.get(id=ml_id)
    today = str(datetime.today().date())
    response = HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="'
            + mail_list.name
            + "-list-CSV-export"
            + today
            + '.csv"'
        },
    )

    writer = csv.writer(response)
    writer.writerow(["Name", "Email", "Mailing List", "List ID"])
    for item in mail_list.users.all():
        writer.writerow(
            [
                item.first_name + item.last_name,
                item.email,
                mail_list.name,
                mail_list.email,
            ]
        )
    if mail_list.guest_user.all is not None:
        for guest in mail_list.guest_user.all():
            writer.writerow(
                [
                    guest.user,
                    guest.email,
                    mail_list.name,
                    mail_list.email,
                ]
            )
    return response


@login_required
@require_http_methods(["GET", "POST"])
def mailing_list_new(request, ml_id=None):
    employee = IFPUser.objects.get(id=request.user.id)
    mail_list = MailingList()
    if request.method == "GET" and request.user.is_superuser:
        form = NewMailingListForm(instance=mail_list)
        return render(
            request,
            "employee/mailing_list_new.html",
            {"mail_list": mail_list, "form": form,"employee":employee},
        )
    else:
        form = NewMailingListForm(request.POST, instance=mail_list)
        if form.is_valid():
            mail_list = form.save()
            print(mail_list)
        return redirect(reverse("mailing_list_view", kwargs={"ml_id": mail_list.id}))


@login_required
@require_POST
def employee_ml_update(request, empid):
    employee = IFPUser.objects.get(id=empid)
    mailinglist = MailingList.objects.all()
    if request.method == "POST" and request.user.is_superuser:
        values = request.POST
        employee.mailinglist_set.clear()
        for a in values:
            if a == "csrfmiddlewaretoken":
                continue
            ml = MailingList.objects.get(name=a)
            employee.mailinglist_set.add(ml)
            employee.save()
        return redirect(reverse("mailing_list_list"))
    return redirect(reverse("mailing_list_list"))


@login_required
@require_GET
def mailing_list_export_all(request):
    mail_list = MailingList.objects.all()
    today = str(datetime.today().date())
    ml_values = [i.name for i in mail_list]
    user_list = IFPUser.objects.all()
    response = HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="'
            + "mailing-full-list-CSV-export"
            + today
            + '.csv"'
        },
    )
    writer = csv.writer(response)
    writer.writerow(["Name", "Email"] + ml_values)
    for user in user_list:
        export = []
        export.append(user.first_name + user.last_name)
        export.append(user.email)
        for i in ml_values:
            a = MailingList.objects.get(name=i)
            if a in user.mailinglist_set.all():
                export.append("Yes")
            else:
                export.append("No")
        writer.writerow(export)
    return response


@login_required
@require_GET
def leave_list(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "GET":
        if request.user.is_superuser:
            leave_data = Leave.objects.all().order_by("-from_date")
        return render(
            request,
            "employee/leave_list.html",
            {"leaves": leave_data,"employee":employee},
        )

@login_required
@require_http_methods(["GET", "POST"])
def my_leave(request):
    leave = Leave()
    employee = IFPUser.objects.get(id=request.user.id)
    if (request.user.is_superuser or employee.contract_type == 'CDD' or employee.contract_type == 'CDI'):
        remaining_leave = int(employee.casual_leaves)
        default_leave = employee.default_casual_leaves
        leaves_taken = default_leave - remaining_leave
        eleaves = Leave.objects.filter(applied_by=request.user.id).order_by("-from_date")
        form = LeaveDocument(request.POST, request.FILES, instance=leave)
        if request.method == "POST":
            leave_id = request.POST["leave-id"]
            current_leave = Leave.objects.get(id=leave_id)
            if form.is_valid():
                current_leave.leave_doc = form.cleaned_data['leave_doc']
                current_leave.save()
        return render(
            request,
            "employee/my_leave.html",
            {"form":form,"employee":employee,"eleaves": eleaves,"remaining_leave":remaining_leave,"leaves_taken":leaves_taken, "default_leave":default_leave},
        )
@login_required
@require_POST
def leave_withdraw(request):
    employee = IFPUser.objects.get(id=request.user.id)
    if request.method == "POST":
        leave_id = request.POST["leave-id"]
        leave = Leave.objects.get(id=leave_id)
        days = leave.days
        if leave.leave_type == "Casual":
            employee.casual_leaves = employee.casual_leaves + days
        elif leave.leave_type == "Medical":
            employee.medical_leaves = employee.medical_leaves + days
        employee.save()
        leave.delete()
        return redirect(reverse("my_leave"))    


@login_required
@require_GET
def leave_export(request):
    leaves = Leave.objects.all()
    today = str(datetime.today().date())
    response = HttpResponse(
        content_type="text/csv",
        headers={
            "Content-Disposition": 'attachment; filename="'
            + "leave"
            + "-summary-CSV-export"
            + today
            + '.csv"'
        },
    )

    months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    head = ["Employee Name"]
    for month in months:
        head.append(month + " - Casual Leave")
        head.append(month + " - Medical Leave")
        head.append(month + " - Total Leave")
    head.append("Total casual leaves taken")
    head.append("Remaining casual leaves")
    head.append("Todal medical leaves taken")
    head.append("Remaining medical leaves")

    writer = csv.writer(response)

    writer.writerows(
    [
        head,

    ]
    )
    int_months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    employee = IFPUser.objects.all()

    for emp in employee:
        body = [emp]
        total_casual = 0
        total_medical = 0
        for month in int_months:
            casual_leave_count = 0
            medical_leave_count = 0
            for leave2 in leaves:
                if month == leave2.from_date.month:
                    if leave2.applied_by == emp:
                        for x in range(int(leave2.days)):
                            temp = leave2.from_date + timedelta(days=x)
                            if leave2.from_date.month == temp.month and leave2.leave_type== "Casual":
                                casual_leave_count = casual_leave_count + 1
                            elif leave2.from_date.month == temp.month and leave2.leave_type== "Medical":
                                medical_leave_count = medical_leave_count + 1
            body.append(casual_leave_count)
            body.append(medical_leave_count)
            body.append(casual_leave_count+medical_leave_count)
            total_casual = total_casual + casual_leave_count
            total_medical = total_medical + medical_leave_count
        body.append(total_casual)
        body.append(emp.casual_leaves)
        body.append(total_medical)
        body.append(emp.medical_leaves)
        writer.writerows(
            [
                body,

            ]
        )

   
    return response

@login_required
@require_http_methods(["GET", "POST"])
def reset_password(request):
    pass


